<?php
    use App\Models\Role;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Kiểm sát viên')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active">Kiểm sát viên</li>
        <li class="breadcrumb-item active">Danh sách kiểm sát viên</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
            </div>
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div id="table-users_filter" class="dataTables_filter">
                            <label>
                                <input type="search" class="form-control form-control-sm filter-group" placeholder="Tìm kiếm..." aria-controls="table-users">
                            </label>
                        </div>
                        <div class="dt-buttons btn-group">
                            <button class="btn btn-secondary action-item add-surveyor" tabindex="0" aria-controls="table" data-toggle="modal" data-target="#modal-surveyor">
                                <a href="#">
                                    <i class="fa fa-plus"></i> Thêm mới
                                </a>
                            </button>

                            <button class="btn btn-secondary buttons-reload" tabindex="0" aria-controls="table-users" onclick="reLoadTable()">
                                <span><i class="fas fa-sync"></i> Tải lại</span>
                            </button>
                        </div>
                        <table class="table table-striped table-hover vertical-middle dataTable no-footer" id="table" role="grid" aria-describedby="table-users_info" style="width: 1633px;">
                            <thead>
                                <tr role="row">
                                    <th width="10px" class="text-left no-sort sorting_disabled" rowspan="1" colspan="1" style="width: 37px;" aria-label="">
                                        <input class="table-check-all" data-set=".dataTable .checkboxes" type="checkbox">
                                    </th>
                                    <th title="STT" class="text-left column-key-username sorting_desc" style="width: 50px;">STT</th>
                                    <th title="Name" class="text-left column-key-email sorting" style="width: 302px;">Tên</th>
                                    <th title="Email" width="100px" class="column-key-created_at sorting" style="width: 100px;">Email</th>
                                    <th title="Status" width="100px" class="column-key-created_at sorting" style="width: 100px;">Trạng thái</th>
                                    <th title="Created_at" width="100px" class="column-key-status sorting" style="width: 100px;">Thời gian tạo</th>
                                    <th title="Update_at" width="100px" class="column-key-status sorting" style="width: 100px;">Thời gian sửa</th>
                                    <th title="Operations" width="350px" class="text-right sorting_disabled" style="width: 50px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($surveyor) && count($surveyor)>0)
                            @foreach($surveyor as $key => $item)
                                <tr role="row" class="odd">
                                    <td class=" text-left no-sort">
                                        <div class="text-left">
                                            <div class="checkbox checkbox-primary table-checkbox">
                                                <input type="checkbox" class="checkboxes" name="id[]" value="1">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-left column-key-username sorting_1">
                                        {{ ++$key }}
                                    </td>
                                    <td class="text-left column-key-email">
                                        <a class="title-email">{{ $item->name }}</a>
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->email }}
                                    </td>
                                    <td class="column-key-role_name">
                                        @if($item->status==1)
                                            <span class="label btn-primary-my label-many">Hoạt động</span>
                                        @else
                                            <span class="label btn-danger label-many">Khóa</span>
                                        @endif
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->created_at }}
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->updated_at }}
                                    </td>
                                    <td class=" text-right">
                                        <a href="" data-id="{{ $item->id }}" class="btn btn-primary-my update-surveyor" data-toggle="modal" data-target="#modal-surveyor"><i class="fa fa-edit"></i></a>
                                        <a href="" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-toggle="modal" data-target="#popup-delete" class="btn btn-icon btn-danger deleteDialog"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="datatables__info_wrap">
                            <div class="dataTables_info pull-left" id="table_info">
                                <span class="dt-length-records">
                                    Tổng {{ $surveyor->total() }} đối tượng/ {{ $surveyor->lastPage() }} trang
                                </span>
                            </div>
                            <div class="paging_simple_numbers pull-right" id="table_paginate">
                                {!! $surveyor->links() !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.global.popupDelete')

    <!-- Modal surveyor -->
    <div id="modal-surveyor" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-modal my-modal">
                    <h4 class="modal-title">
                        <i class="til_img"></i>
                        <strong>Sửa điều tra viên</strong>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="txtId">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label required">Tên</label>
                            <input class="form-control" name="txtName" type="text" value="{!! old('txtName') !!}" id="name">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Số điện thoại</label>
                            <input class="form-control" name="txtPhone" type="text" value="{!! old('txtPhone') !!}" id="phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Email</label>
                            <input class="form-control" name="txtEmail" type="text" value="{!! old('txtEmail') !!}" id="email">
                        </div>
                        <div class="form-group">
                            <label class="control-labe">Chức danh</label>
                            <input class="form-control" name="txtTitles" type="text" value="{!! old('txtTitles') !!}" id="titles">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <select class="form-control select-status" id="status" name="txtStatus">
                                <option value="1">Hoạt động</option>
                                <option value="2">Khóa</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success my-btn-default btnAddPer" id="btnAddSurveyor">
                        <i class="fa fa-save"></i>
                    </button>
                    <button type="button" class="btn btn-danger my-btn-default" data-dismiss="modal">
                        <i class="fa fa-times-circle"></i>Hủy bỏ
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal surveyor -->

    <script>
        const urlGetSurveyor = '/admin/surveyor/getSurveyor';
        const urlEdit = '/admin/surveyor/edit';
        const urlList = '/admin/surveyor/list';
        const urlDelete = '/admin/surveyor/delete';
        const urlAdd = '/admin/surveyor/add';

        //show dialog add
        $(".add-surveyor").click(function(e) {
            e.preventDefault();
            //prosess form true is insert, false is update
            frmPerform(true);
        });

        //show dialog update
        $('#table tbody').on('click', '.update-surveyor', function() {
            //prosess form true is insert, false is update
            self = this;
            frmPerform(false, self);
        });

        //load form true is insert, false is update
        function frmPerform(bln, self = null) {
            let txtTitle = "Thêm khảo sát viên";
            $("input[name='txtName']").val("");
            $("input[name='txtPhone']").val("");
            $("input[name='txtEmail']").val("");
            $("input[name='txtTitles']").val("");
            $("input[name='txtStatus']").val("");
            $("input[name='txtId']").val("");
            if(!bln) {
                txtTitle = "Sửa khảo sát viên";
                let data = {
                    "id": $(self).data("id")
                }
                let result = ajaxQuery(urlGetSurveyor, data, 'POST');
                if(result.code==200) {
                    $("input[name='txtName']").val(result.data.name);
                    $("input[name='txtPhone']").val(result.data.phone);
                    $("input[name='txtEmail']").val(result.data.email);
                    $("input[name='txtTitles']").val(result.data.titles);
                    $("input[name='txtId']").val(result.data.id);
                    $(".select-status").val(result.data.status);
                } else if(result.code==401) {
                    //noti
                    showAlert(result.message, result.notify, 5000);
                }

            }
            //edit title form
            $('#modal-surveyor .modal-title strong').html(txtTitle);
            //edit button form
            $("#btnAddSurveyor").contents().last()[0].textContent = txtTitle;
        }

        //add surveyor
        $("#btnAddSurveyor").click(function(e) {
            e.preventDefault();
            let _token = $('meta[name="csrf-token"]').attr('content');
            let txtId = $("input[name='txtId']").val();
            let txtName = $("input[name='txtName']").val();
            let txtPhone = $("input[name='txtPhone']").val();
            let txtEmail = $("input[name='txtEmail']").val();
            let txtTitles = $("input[name='txtTitles']").val();
            let txtStatus = $(".select-status").val();
            let data = {
                "_token": _token,
                id: txtId,
                name: txtName,
                phone: txtPhone,
                email: txtEmail,
                titles: txtTitles,
                status: txtStatus,
            };
            //query add permission
            let result = ajaxQuery(txtId=='' ? urlAdd : urlEdit, data, 'POST');
            if(result.code == 200) {
                //reload table
                reLoadTable();

                //close modal add
                $('#modal-surveyor').modal('hide');
                $('.modal-backdrop').remove();

                //noti
                showAlert(result.message, result.notify, 5000);
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            } else {
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        });

        //prosess delete
        $(".btnDel").click(function(e) {
            var data = {
                "id": $(self).data("id"),
                "_token": $('meta[name="csrf-token"]').attr('content'),
            };
            //query del permission
            let result = ajaxQuery(urlDelete, data, 'POST');
            processResponse(result);
        });

        function processResponse(result) {
            if(result == undefined) {
                //noti
                showAlert("@Lang('global.msg_error')", "danger", 5000);
            }
            else if(result.code == 200) {
                //reload table
                reLoadTable();
                //noti
                showAlert(result.message, result.notify, 5000);

                //close modal add
                $('.modal').modal('hide');
                $('.modal-backdrop').remove();
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            }else{
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        }

        $(".filter-group").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                reLoadTable();
            }
        });

        $(document).on('click', '.pagination a', function(){
            let page = $(this).attr('href').split('page=')[1];
            reLoadTable(page);
            return false;
        });

        //reload table
        function reLoadTable(page = null) {
            let data = {
                "txt_search":  $('.filter-group').val()
            }
            let url = page==null ? urlList : urlList + '?page=' + page;

            let html = '';
            let arrResult = ajaxQuery(url, data, 'GET');
            if(arrResult.data.data.length > 0) {
                for(let i=0; i<arrResult.data.data.length; i++){
                    let stt = i + 1;
                    let item = arrResult.data.data[i];
                    let status = '<span class="label btn-primary-my label-many">Hoạt động</span>';
                    if(item.status != 1) status = '<span class="label btn-danger label-many">Khoá</span>';;
                    html +=
                        '<tr role="row" class="odd">' +
                        '<td class=" text-left no-sort">' +
                        '<div class="text-left">' +
                        '<div class="checkbox checkbox-primary table-checkbox">' +
                        '<input type="checkbox" class="checkboxes" name="id[]" value="1">' +
                        '</div>' +
                        '</div>' +
                        '</td>' +
                        '<td class="text-left column-key-stt sorting_1">' + stt + '</td>' +
                        '<td class="text-left column-key-name"><a class="title-name">' + item.name + '</a></td>' +
                        '<td class="column-key-role_name">' + item.email + '</td>' +
                        '<td class="column-key-role_name">' + status + '</td>' +
                        '<td class="column-key-role_name">' + formatDatetime(item.created_at) + '</td>' +
                        '<td class="column-key-role_name">' + formatDatetime(item.updated_at) + '</td>' +
                        '<td class=" text-right">' +
                        '<a href="" data-id="' + item.id + '" class="btn btn-primary-my update-surveyor" data-toggle="modal" data-target="#modal-surveyor"><i class="fa fa-edit"></i></a>' +
                        '<a href="" data-id="' + item.id + '" data-name="' + item.name + '" data-toggle="modal" data-target="#popup-delete" class="btn btn-danger deleteDialog"><i class="fa fa-trash"></i></a>' +
                        '</td>' +
                        '</tr>';
                }
                $("#table tbody").html(html);

                buildPagination(arrResult.data.current_page, arrResult.data.last_page, arrResult.data.total);
            } else {
                html += '<tr>' +
                    '<td colspan="9" class="text-center">' +
                    '<h2 class="no-result-grid">Không có dữ liệu</h2>' +
                    '</td></tr>';
                $("#table tbody").html(html);
            }
        }

        function buildPagination(current_page, last_page, total) {
            if(last_page > 1) {
                //remove class

                $('#table_paginate').remove();
                $('.datatables__info_wrap .clearfix').remove();

                //
                let html = '<div class="paging_simple_numbers pull-right" id="table_paginate"><ul class="pagination">';
                let num=1;
                if(current_page==1) {
                    html += '<li class="page-item disabled"><span class="page-link">«</span>';
                } else {
                    let num_pre = current_page - 1;
                    html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num_pre+'">«</a></li>';
                }

                for(let i=0; i<last_page; i++) {
                    if(num==current_page) {
                        html += '<li class="page-item active"><span class="page-link">'+num+'</span></li>';
                    } else {
                        html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num+'">'+num+'</a></li>';
                    }
                    num++;
                }

                if(current_page==last_page) {
                    html += '<li class="page-item disabled"><span class="page-link">»</span></li>';
                } else {
                    let num_next = current_page + 1;
                    html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num_next+'">»</a></li>';
                }

                html += '</ul>';
                $('.datatables__info_wrap').append(html);

                $('.datatables__info_wrap').append('<div class="clearfix"></div>');
            }

            //info tabel
            $('.datatables__info_wrap').children().remove();
            // $('#table_info').remove();
            let html = '<div class="dataTables_info pull-left" id="table_info"><span class="dt-length-records">Tổng ';
            html += total + ' đối tượng/ ' + last_page + ' trang</span></div>';
            $('.datatables__info_wrap').append(html);
            $('.datatables__info_wrap').append('<div class="clearfix"></div>');
        }

    </script>
@endsection

