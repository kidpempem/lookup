<!-- Navbar -->
<div class="page-header-top navbar navbar-inverse" role="navigation" xmlns="http://www.w3.org/1999/html">
    <div class="navbar-header page-logo">

        <div class="sidebar-toggle menu-toggler responsive-toggler d-block d-sm-none" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </div>
        <div class="sidebar-toggle menu-toggler d-none d-sm-block">
            <span></span>
        </div>
    </div>

    <div class="top-menu">
        <ul class="nav navbar-nav float-right collapse">

            <li class="dropdown dropdown-user">
                <a href="javascript:;" class="dropdown-toggle dropdown-header-name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img alt="System Admin" class="rounded-circle" src="{{ url('admin/image/avatar.jpg') }}" />
                    <span class="username username-hide-on-mobile"> {{ auth()->user()->name }} </span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li><a href="/admin/settings/profile"><i class="icon-user"></i> Profile</a></li>
                    <li><a href="{{ url('logout') }}" class="btn-logout"><i class="icon-key"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /navbar -->
<style>
    @media screen and (max-width: 767px) {
        .navbar-header .menu-toggler.sidebar-toggle {
            margin: 10px 0;
        }
    }
</style>
