<script src="{{ url('admin/js/jquery.stickytableheaders.js') }}"></script>
<script src="{{ url('admin/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ url('admin/js/core.js') }}"></script>
<script src="{{ url('admin/js/toastr.min.js') }}"></script>
<script src="{{ url('admin/js/jquery.mCustomScrollbar.js') }}"></script>
<script src="{{ url('admin/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ url('admin/ckfinder/ckfinder/ckfinder.js') }}"></script>
<script src="{{ url('admin/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('admin/js/moment.js') }}"></script>
<script src="{{ url('admin/js/moment-timezone-with-data.js') }}"></script>
<script src="{{ url('admin/js/myJs.js') }}"></script>
<script src="{{ url('admin/js/my-ajax.js') }}"></script>
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
    $("#toast-container").delay(3000).slideUp();

</script>

<script type="text/javascript">
    var BotbleVariables = BotbleVariables || {};

    BotbleVariables.languages = {
        tables: {"id":"ID","name":"Name","slug":"Slug","title":"Title","order_by":"Order By","order":"Order","status":"Status","created_at":"Created At","updated_at":"Updated At","description":"Description","operations":"Operations","url":"URL","author":"Author","notes":"Notes","column":"Column","origin":"Origin","after_change":"After changes","views":"Views","browser":"Browser","session":"Session","activated":"activated","deactivated":"deactivated","is_featured":"Is featured","edit":"Edit","delete":"Delete","restore":"Restore","activate":"Activate","deactivate":"Deactivate","excel":"Excel","export":"Export","csv":"CSV","pdf":"PDF","print":"Print","reset":"Reset","reload":"Reload","display":"Display","all":"All","add_new":"Add New","action":"Actions","delete_entry":"Delete","view":"View Detail","save":"Save","show_from":"Show from","to":"to","in":"in","records":"records","no_data":"No data to display","no_record":"No record","confirm_delete":"Confirm delete","confirm_delete_msg":"Do you really want to delete this record?","confirm_delete_many_msg":"Do you really want to delete selected record(s)?","cancel":"Cancel","template":"Template","email":"Email","last_login":"Last login","shortcode":"Shortcode","image":"Image","bulk_changes":"Bulk changes","submit":"Submit"},
        notices_msg: {"create_success_message":"Created successfully","update_success_message":"Updated successfully","delete_success_message":"Deleted successfully","success_header":"Success!","error_header":"Error!","cannot_delete":"Can not delete this record!","no_select":"Please select at least one record to perform this action!","processing_request":"We are processing your request.","error":"Error!","success":"Success!","info":"Info!","enum":{"validate_message":"The :attribute value you have entered is invalid."}},
        pagination: {"previous":"&laquo; Previous","next":"Next &raquo;"},
        system: {
            'character_remain': 'character(s) remain'
        },
    };
    BotbleVariables.authorized = "1";


</script>

