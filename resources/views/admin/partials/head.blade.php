<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>@yield('pageTitle')</title>

<meta name="robots" content="noindex,follow"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />

{{--<link rel="icon shortcut" href="https://cms.botble.com/vendor/core/images/favicon.png">--}}
<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:100%2C100italic%2C300%2C300italic%2C400%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic|Roboto+Slab:100%2C300%2C400%2C700&#038;subset=greek-ext%2Cgreek%2Ccyrillic-ext%2Clatin-ext%2Clatin%2Cvietnamese%2Ccyrillic' type='text/css' media='all'/>

<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/bootstrap.min.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/fontawesome.min.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/simple-line-icons.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/pace-theme-minimal.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/toastr.min.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/jquery.mCustomScrollbar.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/bootstrap-datepicker3.min.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/spectrum.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/jquery.fancybox.min.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/jquery-jvectormap-1.2.2.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/morris.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/core.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/my-style.css') }}">
<link media="all" type="text/css" rel="stylesheet" href="{{ url('admin/css/table.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />


<script src="{{ url('admin/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
