<?php
    use App\Models\Menu_admin;
    $arrPath = explode("/", Request::path());
?>


<!-- Sidebar -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse">
        <div class="sidebar">
            <div class="sidebar-content">
                <ul class="page-sidebar-menu page-header-fixed navigation" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200">

                <?php $menuSettingHtml = Menu_admin::Menu_Settings(null); ?>
                    @if(isset($menuSettingHtml) && count($menuSettingHtml))
                        @foreach($menuSettingHtml as $key => $item)
                            <li class="nav-item <?php if(str_contains($item->slug, $arrPath[1])){ echo 'open';}?>" id="cms-core-dashboard" >
                                <a href="<?php if($key==0) {echo '/'.$item->slug;}else{echo '#';}?>" class="nav-link nav-toggle">
                                    <i class="fa {{ $item->icon }}"></i>
                                    <span class="title">{{ $item->name }} </span>
                                    <span class="arrow "></span>
                                </a>
                                <?php $menuParent = Menu_admin::Menu_Settings($item->id); ?>
                                @if(isset($menuParent) && count($menuParent))
                                <ul class="sub-menu  hidden-ul" <?php if(str_contains($item->slug, $arrPath[1])){ echo 'style="display: block;"';}?>>
                                    @foreach($menuParent as $rec)
                                        <li class="nav-item {{ Request::is($rec->slug) ? 'active' : '' }}">
                                            <a href="{{ url($rec->slug) }}" class="nav-link">
                                                <i class=""></i>
                                                {{ $rec->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /sidebar -->
