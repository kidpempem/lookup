@extends('admin.layouts.auth')
@section('content')

    <div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one" style="background-image: url({{ url('admin/assets/images/auth/login_1.jpg') }}); background-size: cover;">
        <div class="row w-100">
            <div class="col-lg-4 mx-auto">
                <div class="auto-form-wrapper">
                    <h1 class="text-center">TRA CUU THONG TIN</h1>
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="label">Username</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Username" name="username" autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                     <i class="mdi mdi-check-circle-outline"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label">Password</label>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" placeholder="*********">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                      <i class="mdi mdi-check-circle-outline"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary submit-btn btn-block">Login</button>
                        </div>
                        <div class="form-group d-flex justify-content-between">
                            <div class="form-check form-check-flat mt-0">
                                <input name="remmember" class="form-check-input rd-news rd-type" type="checkbox" value="" id="flexCheckDefault">Keep me signed in
                            </div>
                        </div>
                    </form>
                </div>
                <ul class="auth-footer">
                    <li>
                        <a href="#">Conditions</a>
                    </li>
                    <li>
                        <a href="#">Help</a>
                    </li>
                    <li>
                        <a href="#">Terms</a>
                    </li>
                </ul>
                <p class="footer-text text-center">copyright © 2020 . All rights reserved.</p>
            </div>
        </div>
    </div>
@endsection

@push('custom-scripts')
    <script>
        console.log('login');
        var urlLogin = '/login';

        $(".submit-btn").click(function(e) {
            e.preventDefault();
            var _token = $('meta[name="csrf-token"]').attr('content');
            var username = $("input[name='username']").val();
            var password = $("input[name='password']").val();
            let remember = $("input[name='remmember']").is(':checked') ? '1' : '0';

            if(!username || !password) {
                showAlert('Chưa nhập username hoặc password', "danger", 5000);
                return;
            }

            var data = {
                "_token": _token,
                "username": username,
                "password": password,
                "remember": remember
            };
            //query
            let result = ajaxQuery(urlLogin, data, 'POST');
            if(!result) {
                //noti
                showAlert("Có lỗi vui lòng thử lại", "danger", 5000);
            } else {
                //noti
                showAlert(result.message, result.notify, 5000);

                if(result.code==200) window.location.href = "/admin/dashboard";
            }
        });
    </script>
@endpush
