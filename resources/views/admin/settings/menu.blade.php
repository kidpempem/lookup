<?php
use App\Models\Role;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Menu')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Cài đặt</li>
        <li class="breadcrumb-item active">Menu</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
            </div>
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        {!! Menu::render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom-scripts')
    {!! Menu::scripts() !!}
@endpush
