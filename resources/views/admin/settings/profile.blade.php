<?php
use App\Models\Role;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Cài đặt chung')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Cài đặt</li>
        <li class="breadcrumb-item active">Thông tin người dùng</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="user-profile row">
                            <div class="col-md-3 col-sm-5 crop-avatar">
                                <div class="profile-userpic mt-card-item">
                                    <div class="avatar-view mt-card-avatar mt-overlay-1" data-original-title="" title="">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAvg0lEQVR42u3deZwU5YE38N/zVFVX9TU9B3M1c8/AzMCAiICImugaYxLUkA9EN6JG40ajWXWz5ti8m/dNTDyyOddkY9SsSryyKiSu0USjiSaKAqLi3APMATPMwTA9R19V3V31vH+MbRAGmKOrq7vn+X4+/AHT3fV0M79+nnpOAi7tMcZsjDH3aMzwTMT0PNVg+THG8g2GPAA5hMBDACcBFEIgASCTT0OUASoDgoxhHMAoJRgRCRlWKBn2iMJItkjHCSE+Qgiz+n1ys0esLgB3aowxSTOMvD41VhM0jBUUWOUS6Ip8m7jYKVD7VM/p16IY0GI4EtUxHtMR1A2oBkOUMRgMoASQCIFMCVwChUcUsEASUCyL8MrSlOUI6kZ4OBLbG9CNPQaw20npnhJF3C9T6iOERKz+nLgT40FPIYwxAsDdE47UTcSM8xWBXFQqS+vsApXjj2kPanhzPIT3Air2hyI4pMVwJBrDRMzAhG4ktDwiARyUIkukWCCJWCiLqHHYsNylYJ3HgTrnB8VCWDe0Xi36hqqzF7NE+kqF3dZOCJmw+jPlJvGgW4gxRkejekWfFv24TMllix3y+fGfvesP4/kjAewYD6EjFMFgJIZAgoOcCC6BosgmotZhw1qPA+sXuHC6+++NjL0h7RXVYE+VyNKfciXhICEkZnWZ5yMe9CSbiMbKutXopW6BXldpt60AJpvZvx4Yw599QTQHNRyOxJDON8QUQL5NxFKnjI/lOnF1cTYWvn870B2O7PHrxoOVivSsWxT6CCGp9+2VgXjQTcYYU/aFtLOijN20xKlsAoA+NYp7+3x40RdAe1BDyEjnWE+PgxLUOWVclOvCTSW5KFEmg98a1LZJBL9Y5JDfJISoVpczU/Ggm4Ax5mwJqJ+0C/QbVXbbKgDY0j+KRwfHsXsinPB76XSUJVCszrLjyiIPrvHmAAC6wpHdYd34j6Uu5Y+EkKDVZcwkPOgJwhiT24Lax2RKvhMP9x3dw/ifoXG0BzXoVhcwhQkA6p0yLi/04FuV+QAmQ68Z7Dv1TvllQohmdRnTHQ/6HDDGSL8WbRiL6d9e4lQ2AsDdPcN4ZGAMHaFIWt9nW6nOYcPVxdn4ZsVk6FuD6tZsUbh9oWJrtrps6YoHfRYYY67GgHpNtd32A6dA7U8NjePHB0fw9kSY19wJJAA4I8uO28rycFmhB0HdCHeGI19f7lK2EEICVpcvnfCgz4AvEqsaiES/v8SpfFbVDdzQ3o9nhv38njsJsgSKDflu3FfnhV2gaA2qTxfZpH/Ls4ldVpctHfCgnwJjjHSGImdLlDxQpkj1zw5P4PbuYbzj5x3EVlnpVvDtygJcmu/GQTXaFjXY9dUO23Y+TffEeNBPgDFGW4PaJV6buCVbErLv7hnGz3t9GIjw+R6potgm4ubSXHyzIh9jUX2sPxK7ZolT/j0fmz8eD/oxGGO0JaB+ttphe0Sh1Hbr3gE83D8GP2+epyy3QHGtNxv3LC6GahiRzlDkqqUuZSsP/N/xoL+PMUZag9qllYr0pF2g8vVt/XhscAzheTCZJVPYKcHmomz8qt6LsG5o3Wr08iVO+VnepOdBBwDsD2rnZEv0mTxJzLuxvR9bBsag8oCnLYUSXFucjXvrvPBFYyOjUWNDjVN+3epyWWleB30kEqsaj+nbKu22Ff/eOYSf9fpScuEINzsugeLW0lzcUV2I7nBkj0cUNs7XXnpqdQGswBhzNfnDD+dKQuero8EVhX9rx109R3jIM0xAN3BnzxEU/q0dr4wGV+RKQmeTP/wwY8xlddmSbV7V6Iwx0hxQr25wKVvagxo+29SL5iCfXTlfLHXK2LqsFHVOGc0B9ZoGl/LIfLl/nzdB90ViVSHDeGmhLFVtauzFtmG+J8J8tbEgC1uXleKQFu2yU3rhfGjOZ3zTnTEmvDcRvjNHEjp/P+yv8rzaxkM+z207PAHPq234/bC/KlcSOt+bCN/JGBOsLpeZMrpG71cjSx0C3e4RBc+Zb3Vh10TY6iJxKWZNlh07V1dhPKaPh3RjnVextVpdJjNkZI3OGKPvTYS/VyxLzff2+TzKX1p5yLkp7ZoIQ3mlFff2+TzFstTy3kT4DsZYxuUi42r0QExf6I8ZO4pksWTNW114iwecm6Z47T6oxfrcIl3rEoVDVpcpUTLqm6vJH97sFGjfH0f8Ja5XWnnIuRnZNRGG65VW/GHEX+IUaF+TP3yF1WVKlIyo0Rljttag+vgSp7LpM40H8cyw3+oicWluQ74bv1tehtagum2JU7ki3fetT/ugT0RjZQbQGNINz1m7u3FAjVpdJC5DlCsSdqyqgl0g4xRYniWJB60u02ylddO9NaBe7BaFA08Mjnsqtu/jIecS6oAaRfn2vXh8cNzjFoUDrQF1vdVlmq20DDpjjLw3Ef5evVP+/ebmPtzUMYAImxcTnLgkizCGL3cM4MrmPtQ75efe75VPu5Zw2hWYMWbrCGrP1TrlC8/Y1cl3euGSZqVbwdtrqtER1F6qdcoXp9N9e1oF3TCMnOGo3hxjzLtyVxeG+G4vXJIV2kS8s6YKAiH9BZLQQCkdtbpM05E2TXdfJFZFCPHtGA95q9/Yx0POWWIoEkP1G/uwczzsJYT4RiKxKqvLNB1pEfTukLYmRxI6f3TgCDY09vJNIThLqQbDhsaD+NGBI8iVhM7ukLbG6jKdSsoHvS2gfqrCbtt5694BfG3/ED8UgUsJDMDX9g/h1o4BVNhtO9sC6qesLtPJpPQ9epM/fEWDS3n8iuY+/GZo3OricNyUPlfowRMNJWgOqFcuc9sft7o8U0nZoDf6w9cvcyn3X7znAJ4f4YdycKltfZ4Lz60oR1NAvWG52/6A1eU5VkoG/T1/+J+Xu5SfX/BOD/4yyg/V5NLDBTlOvLyyAo0B9ebT3Pb/sro8R0u5oMdD/tG3u/G3sZDVxeG4GflItgN/PaMy5cKeUkGPN9d5yLl0Fg97KjXjUybo8Y433lznMkG8GZ8qHXQpEfS2gPqpOqf8PO944zJJvIOuPaitr3cpf7CyLJYHvTukramw23byITQuE8WH3nrCkTMrHfIuq8phadBHIrGqXEnovHXvAH7W67OyKBxnmltKcnFPbTF8Ub3aqq2lLQu6YRg5hBDfDw8cwdf3D1lVDI5Lih/WFOKr5QvAGMu1YiGMJUFnjNkOR2LdO8fD3k83HuTTWrmMRwA8s7wMZ3rs/YU2sTLZS1yTPtedMUY6gtpzMca8lzX38pBz8wIDcHlzL3TGvB1B7blkb16R9KA3+tXv1TrlC1fu6uKr0Lh5RTUYVu7qQq1TvrDRr34vmddO6rdKa0BdX++Un+M7w3DzWXynmragdvESl/J8Mq6ZtKBPRGNlblE4cGVzHx7nw2jcPLe50IPHGkrgj+nlydhdNilNd8aYzQAa7+3z8ZBzHIDHh8Zxb58PBtDIGLOZfb2kBL01qD4R1g3PV/YOJuNyHJcWvrJ3EGGdeVqD6hNmX8v0oDf5w5uXOJWNa3d38y2ZOe4oEcawdncXljiVjWYf/2TqPXogpi90CrSPH5PEcScWP/4pqBslZh3saFqNzhij/pix46H+UR5yjjuJZ4b9eKh/FP6YscOsI5tNC3qjX/1ukSyW3NIxYN4nxHEZ4paOARTJYkmjX/2uGa9vStO9X40sLZalZn4+OcdNX/x89gEtutSr2FoT+doJr9EZY4JDoG/c3TPMQ85xM7BrIoy7e4bhEOgbjDEhka+d8KA3+tXvekQh6/bu4eR9QhyXIW7vHoZHFDyJbsIntOkeX19+5ltd2MVrc46blXgTPpHr1xNWozPGSNgwXrqvz8dDznFzsGsijPv6fAgbxkuJWuWWsKA3BdSrF8pS1Tf4JhIcN2ff2D+EhbJU1RxQr07E6yXk24Ix5gLg39TUi22HJyz9gDguU2wsyMLWZaUA4CaEzGnXVDERBWoOqD8XCeEh57gE2nZ4Au1BDTHGfg7g2rm81pxrdF8kVpUjCZ3LduxHc1Cz+rPhuIyy1CmjeW3NnDvm5nyPPhbTtz3UP8pDznEmaAlqeKh/FOMxfdtcXmdONfr+oHZOtcP2WuHf2nE4qlv9mXBcRiqQBAx9pA6doci5NU759dm8xqxrdMYYyZboM//eOcRDznEmOhzV8a3OIWRL9JnZDrfNOuitQe3TeZKYxw9e4Djz3dPrQ54k5rUGtUtn8/xZBZ0xRisV6X9uau9HQDes/gw4LuMFdAM3tfejUpGenM1S1lkFvSWgbrILVH54YMzq989x88bDA2OwC1RuCaibZvrcGQedMUarHbZHr2/r5/uyc1wSqQbDF9v6Ue2wPTrTWn3GQW8NapcolNoeGxyz+n1z3Lzz+OAYFEptrUHtkpk8b0ZBZ4wRr03ccmvHAMK8Nue4pAsbDLfuHYDXJm6ZSQ/8jLrq4+PmWa+2wc874VKOS6CotkuotttQbbeh6v0/OZIAJyVwCvSDPzIliDKGiMEQ0hl8MR0j0RgGtBgOqFF0hyNoC2loCmg4wodPU4pboJg4r35G4+ozmusuUfLA3T3DPOQpYqEs4txsB87NduIj2Q4sdcogZPrf3TIhkCngFoFCWQQgT/m4PjWK7eMhvDYWwp9GAtgXTupBoNwx/LqBu3uGsbko+wEAS6bznGn/VsQ3lfC+1oGBSMzq9zpvLXPK2FzkwaYCD6odph/wMaV9IQ3PDPvx+OAY3gvwqc9WKLaJ6D+3dtpz4Kcd9JZA+KnOUPSzlzaafkwUd4xSWcTmomxsLvKgwaVYXZwPaQqouLfPh0cHxhDk/TZJ9b/Ly1DjkJ5e6rJfdqrHTivo8fXm/BTU5FrtVvDV8gXYWJAFYQZNciuMRXX85OAIfnLwCA98ksRPZcU01qtPq9e9MaBeo+oGD3mSnOaS8fvTyrBrTTUuK/SkfMgBIFsS8N3qAuxftwg3LsyBmPpFTnvv+FWEdQONAfWaUz32lP8djDES1I3gTe399kcG+UmoZsoRKe6uLsQXF+aApkG4T2ZvSMPNHQP4ky9odVEy2tVFHtxb5w07BeokhJywKXXKGr1fizY4BWrnxyqZ69ML3Og4axFuKMlN+5ADwGKHjBdWlONHNYWQ0v/tpKxnhv1wCtTer0WXnuxxpwz6WEz/9lND45jgQ2qmkAnB/XXFeOa0MuTbErKzV8oghOC28gV4c1UVFtmtGSHIdBO6gaeGxjEW0799ssed9LuWMSYDUPk+7eYoson47fJSnOVxWF0U0wViOjY29fKmvAni+8ADUAghU453nrRGbwtqHwOAt3nIE65CkbB9VeW8CDkAuEQBz55WhvV5LquLknHi+YzndSonDbpMyXfu7hkGnwCZWBWKhNfPqETVPGvOypTit8tLsSHfbXVRMooO4O6eYciUfOdEjzlh050x5gQQqH9zH9pDfMpjouRLAravqsQihzz3F0tTMYNhQ+NBPD8yp63KuaPUOWxoO2sRALgIIcfdH52wRm8JqJ8AgA4e8oQRCfD0stJ5HXIAECnBY0tLUKlIVhclY8Rz2hJQPznVz08YdLtA/+2O7mHwOU6Jc2dVIT6a47S6GCkhWxKwdVkp5AwYSkwFDMAd3cOwC/QbU/18yqAzxpQqu23V/wzxCTKJss5jx1fL86wuRkpZmWXHPYuLrC5GxnhyaBxVdtsqxthxCyKmDPq+kHYWALTzQxkSQiTAg/ULM2IiTKLdUJKLT/Ke+IRoez+v8fwebcqgRxm7aUv/KO9tT5AbFuaizmntfXnUYBiJxtAVjuBdfxjvTITRGlDRr0WhM2tv0H62uIg34RNAB7ClfxRRxr587M+O+3Tf33ROv+CdHvxllE9umCuZEPScvQhFcvI6nnTG8GdfENvHQ9jjV/GuP4xe7cR7CFAAVXYblrtknJvtxMdznViS5OWwt+0dxE96R5J6zUx0QY4TL6+sAACBEPLBdNbjgj4RjZW5ReGA59U2Pu01Aa7zZuO/6xcm5Vr7QxruOzSKxwfHMTjHzUFOdyn459JcXFWUDYmaX9sejsRQuX0vQnyJ65xkCRTj59XDH9PLsyTxg80jjmu6d6vRS/vUKA95gty4MNf0a4R0A//eOYSlOzrx44Mjcw45ALwbUHFdWz/q3tyHF0bMX9BUYBPx+eJs06+T6SZ0A31qFN1q9EMnuhwXdLdAr7u3jx+zlAi1DhvOyLKbeo1Gv4qGHftxV88RREy41+5So/jknoP4172DiJlc295UYv6X4nxwb58PboFed/S/fSjojDGx0m5b8aKPz1hKhA35Waa+/q7xEM55uxvdatT09/LT3hH8Y3MvoiaGvcGlYJU7tbbKSkcv+gKotNtWMMY+WA75oaD7onoZwIfVEuXCXPMmx/SpUax/72BSd+TdNuzHP3cMmHqNywo9SXs/mSqe33iegWOC3qdFP96vRXmHSAIQAGea2Gz/Ylu/JfutP9A/iidNnEh16QK+4GWuQgbDIS2KPi368fi/fSjoCiWX/ZofnJgQNXYbXKJgymu/OhrECxbeXv3L3kGETGpJ1DplFGXYBhxWeGRgDAoll8f//kHQGWNksUM+/898Y4CEWGTinuv3H7K2s3QwEsOvDo2a9vrnZM+PNfpmetkXxGKHfF782Kaja/QsAGjm9+cJUWLSBBnGGF5IgeWdD/abF/Tlrvm9ui8RWv6eYzdwVNB7wpFaABjmp7AkRJ5kTrP9gBrFWMz6OQ5NQQ37Q+ZUCkudvOd9ruI57glH6oCjgj4RM85/1x+G9b9CmcEumDObLJWOw3p9LGTK65bxdepzZgB41x/GRMw4Hzgq6IpALnr+iPVNwkwhzOyg2mmLpNCISItJt3lemXfGJcLzRwJQBHIRcFTQS2Vp3Y5xc76h5yPVpEDmmnRLMBs9Jk3UyTFptGK+2TEeQqksrQPePzaZMWYDIPNtoxJnQjdnjLvWYYONEFOmu874PcbMeY8KJSAA391ojjpCEdgFKjPGJgcsNcPIlSlNyGIIblK/Zs5naaMUF+Y6U2JjxaaAhhvb+015bR70uYvnWTOMXBEA+tRYTbXDhiBfsZYw3WHzWke3lualRNAHIjHcZ+J4Ojc38Tz3qbEaCgBBw1jRHtT4N2gCtQQ1GCY1ry/Mc/GpotwpMUzOew8axgoKABRY9QbviEuosMFM65UGgP+u96KKD0Nxp/DGeAgUWEUBwCXQ0xsD/OzzRPuLidOJ820i/nR6Bd8bnTupxoAKl0Ana/R8m7hoP+9xT7jnjpi7M0u1w4Y3V1XhH/he8dwJ7A9FkG8TF1PGGHUK1H7IpF7i+eyV0SAOmzySUSiLeOn0cvznoiK4hFOegs3NM4e0GJwCtVMANgA4EuVBTzQdwMMmLv6Io4Tg1rI8dJxVg2uLs0996D03b8RzTd8/TBETKbBQIhP9os9n+l5rcV5ZwkNLFqJlbQ02F3og8q3S5714rulozPAA5k3ZnO96tRgeGkjuWHOdU8ZjDSXoXLcIXy3LQy6fUjpvxXNNJ2J6HgBEU2BKZaa6vWvYkslIZYoNP1xUhEPnLMYjSxbi/ByHSUttuFQVzzVVDZYP8OmGZuqPxPCtziHLrq8IFFcVZ+MvKyvRc/YifL+6AKcn+SQWzhrxXNMoY/n9mvnbBc93P+v14fUx67fpKlNs+EZFPt45sxr7z1qEH9UU4hyPg3fgZbB+LQrKGPIG+NCa6QwAVzT3wZdCoxvVDhtuK1+A11ZVov+cWtxXW4yP5zoh8fZ9RhnQYqAAcq3YNng+6tVi2NRk7iEIs1Uoi7ihJBcvnl6BoXPr8FC9l9/TZ4gjUR2UEGSNm7SumDveK6Mh/FPbIbAU7vzMkQRc683BX1ZW4sDZi3FXdQHqTNzVljPXeEwHJYAzwJenJtUjg+O4sWMgpcMeV6pI+GZFPtrOWoRXVlZgU0EWH59PMwHdgEgAhY+hJ9/9h0ahGQy/qvNCTMKxxIlwXo4T5+U4cUiN4r5DPvyiz4dRPtEq5akGAyUEUiwNapZMtGVgDJe8d9C0LZnMslCR8L3qQhw4ezF+UFPIT1ZJcTHGQAFQXqFb5wVfAGve6krLgy3dooCvlS9Az9mL8JNFhXwGXooy2OQusEaatBwzVkcogtVvdeGhJCyAMYNMKb5StuCDKbcy4b9QqYQQgDKGqMj/YywX0A1c19aPzzQexECaTmDKlgT8cFERmtdW8zXyKUQiBJQBqsKr9JTxzLAf9W/ux319PtP2nDNbjUPGn1dW4KF6L3JEPufOagqdDHqQb1iQWsZ1Azd2DGDlrk78xcLjkefqWm8OGs+swdke886J507NJVBQxjDh4Z0oKem9gIYL3j2AT7zbg7cmwlYXZ1ZKFAmvrqzEN8oX8Fl2FvGIAigA34IUOuaHO96LviDWvNWFT757AH8btX5hzEyJlOD7NYV4dOlCPo/eAgskAZQQjBTzQ+3Swgu+AD76Tg9W7+rEbwbHUnLO/MlsLsrGH1eUI4vfKiZVkSyCSoQMe2W+ZXA62e1XcUXLIZRu78C3OodwwMRTYRLtglwXXjy9HE7eAZw0C2UJVKFkGAC/f0pDQxEdd/YcQdUb+/CJd3vw1NA41DRYt7DW48DvlpfBxod1TRf/hGmWKIwAk2NtXHoyMHkff3lzH4pf78D1bf3422gwpRfNXJjnwn/Xe60uRsaL55oYhpFHCDniebUNE2lQG3DTVyKLuKzAg8sKs7Amyw6Sgl/mN7T144E0nRGYDrIEivHz6kEYYwqAcOnrHejjO81krFJZxKb3Q39mCoVeMwys2tWF5jSc658OSmQRvefUggKIAMACife8Z7JeLYaf9o7grN3dKN++F9/YP4hGv/Xn7cmU4r46L+8jMkk815QQYgR1I7yQD7HNG71aDD84MILTdnVi+Y79uOfgCEYs3Mvu7GwHri3OtvpjyUgLZREh3QhTABiOxPbV8K2C5qWmoIZ/2TcI72t7cVVLH3ZadHz2d6sKeC+8CWocNhyOxPZRAAjoxrvL+T7f81qEMTw2OI61u7ux7q0uPDs8kdRe+4WKhM/zWj3hlrsUBHRjDwUAA9i9zuOwukxcinhzIoxPN/Zi1VtdeHEkeYtqvlaeZ/VbzzjrPA4YwFsUAJyU7qlzyrxDhPuQd/wqPrHnAD793kH0quavkV/kkLE2i690SxSCyXP4nJRO1uglirgfAJx8DjI3hWeP+LFsx3789vCE6de6oshj9dvNGPE8lyjifgoAMqU+AHyTP+6ExnUDm5p6cV+fz9TrbMjPsvqtZox4nmVKfRQACCGRsG5otbznnTsJBuDLHQP4/bDftGuUKhIqFL7IKhFqHTaEdUMjhEQ+aKv3atE31/IOOe4UDABfaDuEUROP8fpINv89TIS1Hgd6teibAPBBW13V2QvrF7jO+79dh60uX9q6tTQXOSbs1vPn0SBeG7NmfHsqR6I6fnjgCO6qKTTl9Ve4FTwyOG7120x76xe4oOrsBeCooGeJ9JUKu21y/2erS5imblyYi1qnnPDXJV2HUyroAPBg/yi+W1Vgyikz1XZ+CzlXFMDpbjt6wpFX4n8HAFTYbR0AkM875GbNrKOtUnE05HBUx06T9rHjQZ+7eI7juT76N2gCAJaaUCPNF2MmHa20MEV3AHrTpOmyBbyymbOGv+d4Ajgq6IQQtjekvfqxXL7x/mwNmLTMt9KemkHvNGkLK779+NxdkOvE3pD2KiGEAR+u0aEa7Ek+33j2ek06YWWZS0nJo4rNasHYBQoe9bn5fHE2VIM9Gf/7hz7PEln6k1eW4OAb982KWQclOgSKle7Umxqqp+5OVfOagxJ4ZQklsvSn+L99KOi5knAQmJwfy82cmbukfCbfbfXbO062SQd/hHSDj/zMQTy/8TwDxwSdEBLrDkf2XJTrsrqsaek9v2raLqyfK/Qg1Y7ZWGTSTEp/jMd8Li7KdaE7HNlDCPmg0+i4WyG/bjx4U0mu1WVNSxpj2GHSkFO53YbLC1Nrwcc5Js1gM+vef764qSQXft148Oh/Oy7olYr0bIki8dM0ZuklEw9FvLO6IGX6T8pkCWtMWlK6L40OpEg1WQJFiSKhUpGePfrfj0uzWxT6AGA1Xxc8K1uHzFvKWWG34fsmTTudqX8ty4Ng0tZPZnVqzgfx3MZzHHdc0AkhRmtQ3XYlXxc8K3vDEezxm3fy6c2lefiCxUOgp7sUmHl718aDPmtXFnnQGlS3EkI+1NExZftcIuQX13hzUq7zJ13cf8jcAwl+Ve/FP3mzLXlvRTYRW5eXQjLxFuKvY+l3YmwqEABc482BRMi9x/5syqAvcshvAkA9H2ablUcHxjBm4jJOSgh+Vb8Qv6gthpzEnVNr7Da8urICVSbORe8MRdAZNn/bqkwUz2s8v0ebMuiEELUrHNmdar286SJoMPy0d8T069xUkouWtTWmj7GLBLilJBe711SZsjrvaC+MmLepRaa7vNCDrnDkLULIcSdznLA6aPaHNy51KVvpn1vAJ0DNnFug6D57EfKSdAJOa0DFz/t82Hp4AkcS1JqwU4JP57vx/yoLkta6W72rE7tT4ASZdEMAGBcsRUtA3dTgtm+b6udTYow5AQTq39yH9hAf7piN6705uD/JJ4bqjOG1sRD+OhrEG+MhtAQ1HJrmYhuRAHUOGae5FHwiz4UN+W64TJr9NpW3J8JY9VZXUj+vTFHnsKHtrEUA4CKEHNfJcdIbvM6QtuvJofHV/6eT7zozGwTAa2dU4myLt0YKxHT0aTEcjsTgi+rQGEPEYLBRgiyBwi1S5IgCahw2yNS6+RPXtBzCrwfHLP2s0tVd1QW4vNCzu9ohr57q5ycNemtAXV/vlJ8T/9wCPldpdmrsNryzpgruJNaM6agpoOL0nZ3892wWBACxC5aiLahdvMSlPD/VY0769V3vlF8GgDP45JlZ2x+O4Ib2AauLkfJu2zfIQz5L8XzG8zqVkwadEKK1BtWtt5Xxo3Lm4jdD4/hBzxGri5GyfjM4hpd8fOx8tm4ry0NrUN1GCDnhTKNTDsIeUiMNXllq8rzahgmTVmbNBwTAbxpKUm5hitU6QxGcvqsTfv67NStZAsX4efXo16LLFiq25hM97pQ9L15ZagnqRnhDCq6HTicMwFUtfUk51ihdhHUDlzf38pDPwYZ8N0K6EfbKUsvJHnfKoBNCWGc48vX765I7TJSJogz4x+Ze/Ib3LCNqMGxs6sXbfMx8Tu6v82J/OPL1+N5wJzKtsZTlLmWLIlCsdPMz1OcqyoDNLYdwZ/ew1UWxjM4Yrmjpwx+TeCRzJlrpVqAIFMtdypZTPXZaQSeEBFqD6tPfqSyw+r1lBAbgW12H8dmmXozPs00WJmI6LnnvILbyW5g5+05lAVqD6tOEkFN+Y057RcRIJFaVKwmd3tc6MBAxZ1vj+ahKkfBYQwnOmgfn3nWHI7jkvYNo4ctQ56zYJqL/3Fr4onp1nk085XTCaU+DyrOJXQfVaNvNpXybqUTqUqM4Z3c3bukYQCCDa/cnBsdwxq5OHvIEubk0FwfVaNt0Qg7MoEYHgP1B7Zxqh+21rFfbeE+pCYptIr5XVYBrvdmgSVx+aqbDkRi+1N6P35l41PJ84xYoJs6rR2cocm6NU359Os+Z0cTmaodt+1hUH7uWH/JgioFIDP/U3o/TdnbiqaFxGCx91w2GdAN3dg9j0Rv7eMgT7NribIxF9bFqh237dJ8z42qjJaB+eolTfsbxSivCJh0qyE1abLfhK2V5uLLIk9RVZHPhj+nYMjCGu3uO8L4cE9gpQej8JWgNahuWupT/ne7zZhx0xhhVDSN8S8eg7Vf95m6ZxE3KEiiuLPLgquJsrE3RTruOoIZ7+3zYMjDGZ1Ca6IveHPystiiiUGo/dl+4k5nVjWCzP3zZUpfypP2VVtOOCuamVqlI+GxhFtbnubHO4zDlfPLpag2o+O2wH9sOT2BPgE98MZtCCcLnL0FLQL28wW1/aibPndVvCWOMhnUjdNu+QfmXJm+EyJ2YR6D4aI4TZ3scWJdtx+luu2lnqccMhvaQhu1jIbz2/p+DJh0qyU3txoU5+PGiIs0uUMdManNglkEH/n6v7n61DQHeVEsJBJM1foNLQY3dhnK7hDJZQoFNRK4kIFcS4KAENkogEQKdTZ4uoxkGNIMhoBsYisQwFNExFInhkBZFR1BDWyiCfSENUd54s4xLoPCfV4/WoPaZpS7lmZk+f9ZBZ4yRkWhs+CcHR/Lu4kswOc5U/6diAf61LG8kTxLzTzWvfSqzbucRQthY1NhwZ3UhCqT06BHmuHRUIAm4s7oQY1Fjw2xCDswh6ABQ45Rf7w5H9tydIscEcVwmurumEN3hyJ7pTo6Zypx7bjyisPEL3hw08MMeOC7hGpwyvuDNQbYobJzL68w56Hk2sas5oG55elmp1Z8Jx2Wcp5eVojmgbsmd5pz2E0nIICxjzAXAv6mpF9v48kOOS4iNBVnYOlmBuqezFPVkEjLoSggJNAXUa7YuK+XnqnNcAmQJFFuXlaIpoF4z15ADCarRgcnhtkNadP/vh/1VN3bw7Y05bi5+WVuMS/LdXQtlqWa2Pe1HS1j1Swhhdkov/FJJLtbwfeA5btbWZNnxpZJc2Cm9MBEhBxIYdGCyY67Rr961c3UVZAvnYHNcupIpwc7VVWj0q3dNd1OJ6Uh4GhljwnhMH7m3z+fhZ7Zx3MzcVV2Am0pyxz2ikEcISdiWQwnvOSOE6CHdWPfNinys5k14jpu21Vl2fLMiHyHdWJfIkAMmBB0AvIqttdGv3rlrdRWcvAnPcafkpAS7Jpvsd3oVW2uiX9+0FDLG6KAWO/CHEX/JdW395n5KHJfmHqz34lN57r4iWSyf6RLU6TBt0JsQYrhFuvYL3hzw45w47sQ25LvxBW8O3CJda0bIARODDgAuUTjUHFA3/255GcoVycxLcVxaKlck/G55GZoD6pUuUThk1nWScgPdElC35orCxvLtexFJ451NOS6RbITgwNmL4Ivp25a67JvMvFZS5qsuccpX2AUy/tPFRcm4HMelhZ8uLoJdoONLnMoVZl8rKUEnhEQosPymklxs5ueDcxw2F3pwU0kuKLCcEBIx+3pJW4GSJYkH24LaxY81lPBTWbl5baVbwWMNJWgLahdnSeLBZFwzqUvNlriU5xv96p1vr6lGoU1M5qU5LiUU2kS8vaYajX71ziUu5flkXTfps1kYY6QjqL3oEumFNW/s4/vCc/OGQgk61y2CP2a8VOuUL0rUgpXpSPricUIIq3XKF4uE9D/VUJr8bxqOswAB8FRDKURC+mud8sXJDDlgQdCByc65AklouCTfjR/wjSW5eeA/agpxSb4b+ZLQkIzOt2NZth0MpXTUF9Wrv1q+ALfwM9e5DHZLaS6+Vr4AvqheTSm15GgjS/d9yrOJXT3hyJn3LC7G5/iwG5eBPlfowT2Li9ETjpyZyPXlM2X5Bm+VDnlXe1Bb/0RDCdbnuawuDsclzPo8F55oKEF7UFtf6ZB3WVkWy4MOAPUu5Q/NAfXK51aU44Icp9XF4bg5uyDHiedWlKM5oF5Z71L+YHV5UiLoALDMbX+8KaDe8PLKCnwkOzXPAOe46fhItgMvr6xAU0C9YZnb/rjV5QFSKOgAsNxtf6AxoN781zMqedi5tPSRbAf+ekYlGgPqzcvd9gesLk9cSgUdAE5z2/8rHvZ/4M14Lo38Q47zg5Cf5rb/l9XlOVrKzldp9IevX+ZS7r94zwE8PzLn/es5zlTr81x4bkU5mgLqDalUk8elbNABoMkf3tzgUh67orkPvxkat7o4HDelzxV68ERDCZoD6pWpck9+rJQOOgC0BdRP1Tnl52/tGMDP+nxWF4fjPuSWklzcU1uM9qC2PhV6108k5YMOAN0hbU2F3bbzRweO4Ov7h8CXwXBWIwB+UFOIr5YvQE84cqbV4+TTKW9a8EViVTmS0Pns8AQub+7jq944yyiU4MmGElyan4XRqF491yONkyFtgg4AhmHkHI7qzTpj3pW7ujAUiVldJG6eKbSJeGdNFQRC+gskocGqueszlXLDayctLKWjhTax0h8zXho8t5bvVMMl1Uq3gsFza+GPGS8V2sTKdAk5kGZBByaXuNY65Ysa/eodb6+pxhV8MQyXBFcUeuI7w9zx/qYRSV9qOhdp1XQ/VmtAXV/vlJ/7ZZ8P/7J3kG8lzSWcjRD85+Ii3FiSi7agdnEyt39KpLQOOgBMRGNlBtAY1g3P2t3dOKBGrS4SlyHKFQk7VlXCLtAJCixL1kaOZki7pvuxsiTxoEcUCnwxfWvP2Yv58U9cQmzId6Pn7MXwxfStHlHIT+eQAxlQox8tPpPu4f5R3NwxgCAfguNmyEkJfl5bjGu9OSk9022mMiroABCI6Qv9MWNHkSyWnPlWF3ZNhK0uEpcm1mTZsXN1FQa1WJ9bpGvNPAst2dK+6X4slygcKpLF8ka/esfO1VW4q7oAMj+jnTsJmRDcVV2AnZPnk99RJIvlmRRyIANr9KP1q5ElDoG+4REFD6/duanEa/HxmD4e0o11XsXWanWZzJBxNfrRvIqt1SMKeY1+9a6dq6vwy9piZAkZ/Za5acoSKH5ZWxyvxe/yiEJepoYcyPAa/WgjkVhV2DBeWihLVZuaerHt8ITVReIssjE/C1uXl+KQFu1yUHphOsxVn6t5E3Rg8jio5oB6dYNL2dIe1LCpqRctQc3qYnFJ0uCU8fSyUtQ5ZTQH1GsaXMojyT4xxSrzqh1LCGHL3PZfA3DHGNvSvLYGD9V7USAJVheNM1GBJOChei+a1tYgxtgWAO5lbvuv50vIgXlWox9rJBKrGo/p2yrtthX/3jmEn/X6ENANq4vFJYhLoLilNBd3VheiOxzZ4xGFjVYeomCleR30uP1B7ZxsiT6TJ4l5N7b3Y8vAGF/vnsYUSnBNcTZ+WefFSDQ2MhY1NtQ45detLpeVeNDfxxgjrUHt0kpFetIuUPn6tn48NjiGMA982rBTgiuLsvFAvRdh3dC61ejlS5zys/OpiX4iPOjHYIzRloC6qdphe1Sh1Hbr3gE83D8GP2/Spyy3QHGtNxv3LC6GahiRzlDkqqUuZSshhP+nvY8H/QQYY7Q1qF3qtYkPZ0tC9t09w/h5rw8DfFeblFFsE3FzaS6+WZGPsag+1h+JXft+Dc4Dfgwe9FNgjJHOUORsiZIHyhSp/tnhCdzePYx3/KrVRZu3VroVfLsyH5fmZ+GgGm2LGuz6aodtO2+inxgP+gz4IrGqgUj0+0ucymfDuoEvtffjmWE/Jniz3nRZAsWGfDfur/NCEShag+rTxTbp3+bDZJdE4EGfBcaYqzGgXlNtt/3AKVD7U0Pj+PHBEbw9EYZudeEyiADgjCw7bivLw2WFHgR1I9wZjnx9uUvZQgjhx/fMAA/6HDDGSL8WXToW07+9xKlsAoC7e4bxyMAYOkIRvv/8LBAAtQ4bri7Oxjcr8gEArUF1W7Yo3O6VpWbePJ8dHvQEYYzJbUHtYzIl36my21YBwB3dw3hyaBxtQY3X9CchAKh3yri80INvVU6Guysc2a0Z7PZ6p/wSIYTPU54jHnQTMMacLQH1k3aBfiMe+i39o3h0cBy7J8L8nh6T99yrsuy4qsiDa7w5ACbDHdaN/1jqUv5ICAlaXcZMwoNuMsaYsi+knRVl7MtLnMpGAOhVo/hlnw8v+gJoD2oIzYNJOQ5KUOeUcVGuCzeV5KJEkQBMNsslQn6xyCG/SQjhQxkm4UFPIsYY9cf0km41eqlboNdV2m0rAKBfi+LXA2N42RdES1DDcCSGdK7zKYB8m4ilThkfy3Xi88XZ8MqTwe4OR/b4dePBSkV61i0KfXzMOzl40C3EGBNHo3pZnxb9uEzJZYsd8vnxn73rD+P5IwHsGA+hIxTBYCSWkgtuXAJFkU1ErcOGtR4H1i9w4XS3/YOf7w1pr2gGe6pElv6UIwkHCSF8xpEFeNBTDGMsqyccqZuIGecrArmoVJbW2QUqx3/eHtTw5ngI7wVU7A9FcEiL4Ug0homYgYBuJLwlkCVQZIkUCyQRC2URNQ4bTnMpOMvjQJ3zg2IhrBtarxZ9Q9XZi1kifaXCbmsnhPDdPVIED3oaYIwRzTAK+9RYTdAwVlBglUugK/Jt4mKnQO1TPadfi2JAi+FIVMd4TEdQN6AaDFHGYDCAEkAiBAolcAoU2aKAPElAsSx+0Mw+VlA3wsOR2N6AbuwxgN1OSveUKOJ+mdJhQggfWEhh/x+mkZlqQEe1QgAAAABJRU5ErkJggg==" class="img-fluid" alt="avatar">
                                    </div>
                                    <div class="mt-card-content">
                                        <h3 class="mt-card-name">{!! isset($data) ? $data['name'] : null !!}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-7">
                                <div class="profile-content">
                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a href="#tab_1_1" class="nav-link active" data-toggle="tab" aria-expanded="true">Thông tin người dùng</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_1">
                                                <div id="profile-form" class="row">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <input type="hidden" name="txtId" value="{!! isset($data) ? $data['id'] : null !!}">
                                                    <div class="form-group col-md-6">
                                                        <label for="first_name" class="control-label required">First Name</label>
                                                        <input class="form-control" data-counter="30" name="first_name" type="text" id="first_name" value="{!! isset($data) ? $data['FirstName'] : null !!}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="last_name" class="control-label required">Last Name</label>
                                                        <input class="form-control" data-counter="30" name="last_name" type="text" id="last_name" value="{!! isset($data) ? $data['LastName'] : null !!}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="email" class="control-label required">Email</label>
                                                        <input class="form-control" placeholder="Ex: example@gmail.com" data-counter="60" name="email" type="text" id="email" value="{!! isset($data) ? $data['email'] : null !!}">
                                                    </div>
                                                    <div class="form-group col-md-6"></div>
                                                    <div class="form-group col-md-6">
                                                        <label for="password" class="control-label">New Password</label>
                                                        <input class="form-control" data-counter="60" name="password" type="password" id="password">
                                                        <div class="pwstrength_viewport_progress">
                                                            <span class="hidden">Password Strength</span> </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="password_confirmation" class="control-label">Confirm New Password</label>
                                                        <input class="form-control" data-counter="60" name="password_confirmation" type="password" id="password_confirmation">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group col-12">
                                                        <div class="form-actions">
                                                            <div class="btn-set text-center">
                                                                <button type="submit" name="submit" value="submit" class="btn btn-success save-data">
                                                                    <i class="fa fa-save"></i> Update
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .mt-overlay-1 {
            cursor: default;
            float: left;
            height: 100%;
            overflow: hidden;
            position: relative;
            text-align: center;
            width: 100%;
            margin-bottom: 15px;
        }
        .mt-card-content {
            text-align: center;
        }
    </style>
    <script>
        const urlEdit = '/admin/settings/editProfile';

        //save data
        $(".save-data").click(function(e) {
            e.preventDefault();

            let _token = $('meta[name="csrf-token"]').attr('content');
            let txtId = $("input[name='txtId']").val();
            let txtFirstName = $("input[name='first_name']").val();
            let txtLastName = $("input[name='last_name']").val();
            let txtEmail = $("input[name='email']").val();
            let txtPassword = $("input[name='password']").val();
            let txtPassConfirm = $("input[name='password_confirmation']").val();

            let data = {
                "_token": _token,
                "id": txtId,
                "FirstName": txtFirstName,
                "LastName": txtLastName,
                "Email": txtEmail,
                "Password": txtPassword,
                "Password_Confirm": txtPassConfirm
            };

            let result = ajaxQuery(urlEdit, data, 'POST');

            if(result == undefined || result.length==0) {
                //noti
                showAlert("@Lang('global.msg_error')", "danger", 5000);
            }
            else if(result.code == 200 || result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);

                if(result.code == 200) {
                    $('.mt-card-name').text(txtFirstName + ' ' + txtLastName);
                }
            } else {
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        });
    </script>
@endsection
