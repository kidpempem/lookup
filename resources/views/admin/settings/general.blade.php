<?php
use App\Models\Menus;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Cài đặt chung')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Cài đặt</li>
        <li class="breadcrumb-item active">Chung</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Thông tin chung</h2>
                                </div>
                                <div class="annotated-section-description pd-all-20 p-none-t">
                                    <p class="color-note">Cài đặt thông tin website</p>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="txtId" value="{!! isset($data) ? $data['id'] : null !!}">
                                    <div class="form-group">
                                        <label for="Name" class="control-label">Tên công ty</label>
                                        <input class="form-control" id="company_name" placeholder="Tên công ty" name="txtCompanyName" type="text" value="{!! isset($data) ? $data['name'] : null !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Email" class="control-label">Email</label>
                                        <input class="form-control" id="Email" placeholder="Email" name="txtEmail" type="text" value="{!! isset($data) ? $data['email'] : null !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Address" class="control-label">Địa chỉ</label>
                                        <input class="form-control" id="Address" placeholder="Địa chỉ" name="txtAddress" type="text" value="{!! isset($data) ? $data['address'] : null !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone" class="control-label">Số điện thoại</label>
                                        <input class="form-control" id="Phone" placeholder="Số điện thoại" name="txtPhone" type="text" value="{!! isset($data) ? $data['phone'] : null !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone" class="control-label">Slogan</label>
                                        <input class="form-control" id="Slogan" placeholder="Slogan" name="txtSlogan" type="text" value="{!! isset($data) ? $data['slogan'] : null !!}">
                                    </div>
                                    <div class="form-group" id="intro_wrap">
                                        <label for="Phone" class="control-label">Giới thiệu</label>
                                        <div class="div-intro">
                                            <a href="#" class="show-intro">Sửa giới thiệu</a>
                                            <div class="intro_edit">
                                                <textarea class="form-control content" name="description" id="description" name="txtContent">{!! isset($data) ? $data['description'] : null !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone" class="control-label">Nhúng google map</label>
                                        <textarea class="form-control content" name="map" id="map" name="txtMap">{!! isset($data) ? $data['gg_map'] : null !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Menu</h2>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                    <div class="form-group">
                                        <label for="Phone" class="control-label">Menu top</label>
                                        <div class="ui-select-wrapper">
                                            <select class="form-control ui-select ui-select is-valid select-status" id="menu_top" name="menu_top">
                                                <option value="0">Chọn menu</option>
                                                <?php $menu = Menus::getAllMenu();?>
                                                @if(!is_null($menu))
                                                @foreach($menu as $item)
                                                    <option value="{{ $item->id }}" {!! $item->id==$data['menu_top'] ? 'selected' : null !!}>{{ $item->name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone" class="control-label">Menu left</label>
                                        <div class="ui-select-wrapper">
                                            <select class="form-control ui-select ui-select is-valid select-status" id="menu_left" name="menu_left">
                                                <option value="0">Chọn menu</option>
                                                <?php $menu = Menus::getAllMenu();?>
                                                @if(!is_null($menu))
                                                    @foreach($menu as $item)
                                                        <option value="{{ $item->id }}" {!! $item->id==$data['menu_left'] ? 'selected' : null !!}>{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Phone" class="control-label">Menu footer</label>
                                        <div class="ui-select-wrapper">
                                            <select class="form-control ui-select ui-select is-valid select-status" id="menu_footer" name="menu_footer">
                                                <option value="0">Chọn menu</option>
                                                <?php $menu = Menus::getAllMenu();?>
                                                @if(!is_null($menu))
                                                    @foreach($menu as $item)
                                                        <option value="{{ $item->id }}" {!! $item->id==$data['menu_footer'] ? 'selected' : null !!}>{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Video tin tức</h2>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                <div class="form-group">
                                        <label for="Phone" class="control-label">Nhúng link video</label>
                                        <textarea class="form-control content" name="linkvideo" id="linkvideo" name="txtLinkvideo">{!! isset($data) ? $data['link_video'] : null !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Logo</h2>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                    <div class="form-group">
                                        <label class="text-title-field" for="admin-logo">Logo
                                        </label>
                                        <div class="admin-logo-image-setting">
                                            <div class="image-box">
                                                <input type="hidden" name="admin_logo" value="" class="image-data">
                                                <div class="preview-image-wrapper  preview-image-wrapper-not-allow-thumb ">
                                                    <img src="{!! isset($data) ? $data['logo'] : '/uploads/placeholder_color.png' !!}" alt="img-logo" id="img-logo">
                                                    <a class="btn_remove_image" title="Remove image">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <label class="btn btnBrowse" onclick="setImageLogo();">
                                                        <i class="fas fa-image" aria-hidden="true"></i>
                                                        <span>Chọn ảnh</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-title-field" for="admin-favicon">Favicon
                                        </label>
                                        <div class="admin-favicon-image-setting">
                                            <div class="image-box">
                                                <input type="hidden" name="admin_favicon" value="" class="image-data">
                                                <div class="preview-image-wrapper  preview-image-wrapper-not-allow-thumb ">
                                                    <img src="{!! isset($data) ? $data['favicon'] : '/uploads/placeholder.png' !!}" alt="img-favicon" id="img-favicon">
                                                    <a class="btn_remove_image" title="Remove image">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <label class="btn btnBrowse" onclick="setImageFavicon();">
                                                        <i class="fas fa-image" aria-hidden="true"></i>
                                                        <span>Chọn ảnh</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Banner</h2>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                <div class="form-group">
                                        <label class="text-title-field" for="admin-bannerOne">banner 1
                                        </label>
                                        <div class="admin-logo-image-setting">
                                            <div class="image-box">
                                                <input type="hidden" name="admin_bannerOne" value="" class="image-data">
                                                <div class="preview-image-wrapper  preview-image-wrapper-not-allow-thumb ">
                                                    <img src="{!! isset($data) ? $data['bannerOne'] : '/uploads/placeholder_color.png' !!}" alt="img-logo" id="img-bannerOne">
                                                    <a class="btn_remove_image" title="Remove image">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <label class="btn btnBrowse" onclick="setImageBannerOne();">
                                                        <i class="fas fa-image" aria-hidden="true"></i>
                                                        <span>Chọn ảnh</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-title-field" for="admin-bannerTwo">banner2
                                        </label>
                                        <div class="admin-logo-image-setting">
                                            <div class="image-box">
                                                <input type="hidden" name="admin_bannerTwo" value="" class="image-data">
                                                <div class="preview-image-wrapper  preview-image-wrapper-not-allow-thumb ">
                                                    <img src="{!! isset($data) ? $data['bannerTwo'] : '/uploads/placeholder_color.png' !!}" alt="img-logo" id="img-bannerTwo">
                                                    <a class="btn_remove_image" title="Remove image">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <label class="btn btnBrowse" onclick="setImageBannerTwo();">
                                                        <i class="fas fa-image" aria-hidden="true"></i>
                                                        <span>Chọn ảnh</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Công cụ tìm kiếm tối ưu</h2>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                    <div class="form-group">
                                        <label for="meta_title" class="control-label">Meta Title</label>
                                        <input class="form-control" id="meta_title" placeholder="Meta Title" data-counter="120" name="meta_title" type="text" value="{!! isset($data) ? $data['meta_title'] : null !!}">
                                        <small class="charcounter">(120 character(s) remain)</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_description" class="control-label">Meta Description</label>
                                        <textarea class="form-control" id="meta_description" placeholder="Meta Description" data-counter="155" name="meta_description" type="text">{!! isset($data) ? $data['meta_description'] : null !!}</textarea>
                                        <small class="charcounter">(155 character(s) remain)</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_keyword" class="control-label">Meta Keyword</label>
                                        <input class="form-control" id="meta_keyword" placeholder="Meta Keyword" data-counter="120" name="meta_keyword" type="text" value="{!! isset($data) ? $data['meta_keyword'] : null !!}">
                                        <small class="charcounter">(120 character(s) remain)</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_description" class="control-label">Google Analytics</label>
                                        <textarea class="form-control" id="txtAnalytics" placeholder="Google Analytics" data-counter="255" name="txtAnalytics" type="text">{!! isset($data) ? $data['analytics'] : null !!}</textarea>
                                        <small class="charcounter">(255 character(s) remain)</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation">
                                <div class="annotated-section-title pd-all-20">
                                    <h2>Đối tác</h2>
                                </div>
                            </div>
                            <div class="flexbox-annotated-section-content">
                                <div class="wrapper-content pd-all-20">
                                    <div class="widget-body">
                                        <input id="gallery-data" class="form-control" name="gallery" type="hidden" value="{{ isset($data) ? $data['contacts'] : null }}">
                                        <div class="list-photos-gallery">
                                            <div class="row" id="list-photos-items"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <a onclick="galleryImages();" class="btn_select_gallery">Chọn ảnh</a>&nbsp;
                                            <a onclick="resetImages();" class="text-danger reset-gallery hidden">Reset</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section">
                            <div class="flexbox-annotated-section-annotation"></div>
                            <div class="flexbox-annotated-section-content">
                                <button class="btn btn-info save-data" type="submit"><i class="fa fa-save"></i>Lưu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .gallery_image_wrapper {
            position: relative;
        }
        .widget-body {
            min-height: inherit;
        }
    </style>
    <script>
        const urlEdit = '/admin/settings/editGeneral';
        //show intro
        $(".show-intro").click(function (e) {
            e.preventDefault();
            $('.intro_edit').toggle('show');
        })

        //save data
        $(".save-data").click(function(e) {
            e.preventDefault();

            let _token = $('meta[name="csrf-token"]').attr('content');
            let txtId = $("input[name='txtId']").val();
            let txtCompanyName = $("input[name='txtCompanyName']").val();
            let txtEmail = $("input[name='txtEmail']").val();
            let txtAddress = $("input[name='txtAddress']").val();
            let txtPhone = $("input[name='txtPhone']").val();
            let metaTitle = $("input[name='meta_title']").val();
            let metaDesc = $("#meta_description").val();
            let metaKeyword = $("input[name='meta_keyword']").val();
            let txtAnalytics = $("#txtAnalytics").val();
            let logo = $('#img-logo').attr('src');
            let favicon = $('#img-favicon').attr('src');
            let bannerOne = $('#img-bannerOne').attr('src');
            let bannerTwo = $('#img-bannerTwo').attr('src');
            let txtDesc = CKEDITOR.instances.description.getData();
            let txtSlogan = $("input[name='txtSlogan']").val();
            let txtMap = $("#map").val();
            let txtLinkvideo = $("#linkvideo").val();
            let lstImage = $("input[name='gallery']").val();
            let menu_top = $("#menu_top").val();
            let menu_footer = $("#menu_footer").val();
            let menu_left = $("#menu_left").val();

            let data = {
                "_token": _token,
                "id": txtId,
                "name": txtCompanyName,
                "email": txtEmail,
                "address": txtAddress,
                "phone": txtPhone,
                "metaTitle": metaTitle,
                "metaDesc": metaDesc,
                "metaKeyword": metaKeyword,
                "logo": logo,
                "favicon": favicon,
                "bannerOne": bannerOne,
                "bannerTwo": bannerTwo,
                'analytics': txtAnalytics,
                'desc': txtDesc,
                "slogan": txtSlogan,
                "map": txtMap,
                "linkvideo": txtLinkvideo,
                "contacts": lstImage,
                "menu_top": menu_top,
                "menu_left": menu_left,
                "menu_footer": menu_footer
            };
            //query add category
            let result = ajaxQuery(urlEdit, data, 'POST');

            if(result == undefined || result.length==0) {
                //noti
                showAlert("@Lang('global.msg_error')", "danger", 5000);
            }
            else if(result.code == 200) {
                //noti
                showAlert(result.message, result.notify, 5000);
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            }
        });


        function setImageLogo() {
            setImage(document.getElementById('img-logo'));
        }

        function setImageFavicon() {
            setImage(document.getElementById('img-favicon'));
        }
        function setImageBannerOne() {
            setImage(document.getElementById('img-bannerOne'));
        }
        function setImageBannerTwo() {
            setImage(document.getElementById('img-bannerTwo'));
        }

        //choose avatar image
        function setImage(eleImage) {
            CKFinder.popup({
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        let file = evt.data.files.first();
                        let value_file = eleImage;
                        value_file.src = file.getUrl();
                    });
                }
            });
        }

        loadContacts();
        function loadContacts() {
            let strImage = $("input[name='gallery']").val();
            if(strImage != null) {
                //list image
                let explode = strImage.split(",");
                if(explode.length > 0) {
                    let html = "";
                    explode.forEach(function (e) {
                        html += "<div class='col-md-2 col-sm-3 col-4 photo-gallery-item'>" +
                            "<div class='gallery_image_wrapper'>" +
                            "<img src='" + e + "'>" +
                            "<a class='btn_remove_image' title='Remove image'><i class='fa fa-times'></i></a>" +
                            "</div></div>";
                    });
                    $("#list-photos-items").append(html);
                    //show reset class
                    $(".reset-gallery").removeClass("hidden");
                }

            }
        }
    </script>
    @push('custom-scripts')
        <script>
            ckeditor("description");
        </script>
    @endpush
@endsection

