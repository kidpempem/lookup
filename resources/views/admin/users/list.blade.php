<?php
    use App\Models\Role;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Danh sách người dùng')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active">Hệ thống</li>
        <li class="breadcrumb-item active">Danh sách người dùng</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
            </div>
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div id="table-users_filter" class="dataTables_filter">
                            <label>
                                <input type="search" class="form-control form-control-sm filter-group" placeholder="Tìm kiếm..." aria-controls="table-users">
                            </label>
                        </div>
                        <div class="dt-buttons btn-group">
                            <button class="btn btn-secondary action-item add-user" tabindex="0" aria-controls="table" data-toggle="modal" data-target="#modal-users">
                                <a href="#">
                                    <i class="fa fa-plus"></i> Thêm mới
                                </a>
                            </button>

                            <button class="btn btn-secondary buttons-reload" tabindex="0" aria-controls="table-users" onclick="reLoadTable()">
                                <span><i class="fas fa-sync"></i> Tải lại</span>
                            </button>
                        </div>
                        <table class="table table-striped table-hover vertical-middle dataTable no-footer" id="table" role="grid" aria-describedby="table-users_info" style="width: 1633px;">
                            <thead>
                                <tr role="row">
                                    <th width="10px" class="text-left no-sort sorting_disabled" rowspan="1" colspan="1" style="width: 37px;" aria-label="">
                                        <input class="table-check-all" data-set=".dataTable .checkboxes" type="checkbox">
                                    </th>
                                    <th title="STT" class="text-left column-key-username sorting_desc" style="width: 50px;">STT</th>
                                    <th title="Name" class="text-left column-key-email sorting" style="width: 302px;">Tên</th>
                                    <th title="Email" width="100px" class="column-key-created_at sorting" style="width: 100px;">Email</th>
                                    <th title="Status" width="100px" class="column-key-created_at sorting" style="width: 100px;">Trạng thái</th>
                                    <th title="Created_at" width="100px" class="column-key-status sorting" style="width: 100px;">Thời gian tạo</th>
                                    <th title="Update_at" width="100px" class="column-key-status sorting" style="width: 100px;">Thời gian sửa</th>
                                    <th title="Operations" width="350px" class="text-right sorting_disabled" style="width: 50px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($users) && count($users)>0)
                            @foreach($users as $key => $item)
                                <tr role="row" class="odd">
                                    <td class=" text-left no-sort">
                                        <div class="text-left">
                                            <div class="checkbox checkbox-primary table-checkbox">
                                                <input type="checkbox" class="checkboxes" name="id[]" value="1">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-left column-key-username sorting_1">
                                        {{ ++$key }}
                                    </td>
                                    <td class="text-left column-key-email">
                                        <a class="title-email">{{ $item->name }}</a>
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->email }}
                                    </td>
                                    <td class="column-key-role_name">
                                        @if($item->status==1)
                                            <span class="label btn-primary-my label-many">Hoạt động</span>
                                        @else
                                            <span class="label btn-danger label-many">Khóa</span>
                                        @endif
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->created_at }}
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->updated_at }}
                                    </td>
                                    <td class=" text-right">
                                        <a href="" data-id="{{ $item->id }}" class="btn btn-primary-my update-users" data-toggle="modal" data-target="#modal-users"><i class="fa fa-edit"></i></a>
                                        <a href="" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-toggle="modal" data-target="#popup-delete" class="btn btn-icon btn-danger deleteDialog"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="datatables__info_wrap">
                            <div class="dataTables_info pull-left" id="table-users_info">
                                <span class="dt-length-records">
                                    Tổng {{ $users->total() }} đối tượng/ {{ $users->lastPage() }} trang
                                </span>
                            </div>
                            <div class="paging_simple_numbers pull-right" id="table_paginate">
                                {!! $users->links() !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.global.popupDelete')

    <!-- Modal User -->
    <div id="modal-users" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-modal my-modal">
                    <h4 class="modal-title">
                        <i class="til_img"></i>
                        <strong>Sửa user</strong>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="txtId">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label required">FirstName</label>
                            <input class="form-control" name="txtFirstName" type="text" value="{!! old('txtFirstName') !!}" id="first_name">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">LastName</label>
                            <input class="form-control" name="txtLastName" type="text" value="{!! old('txtLastName') !!}" id="last_name">
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Email</label>
                            <input class="form-control" name="txtEmail" type="text" value="{!! old('txtEmail') !!}" id="email">
                        </div>
                        <div class="form-group">
                            <label class="control-label label-password">Mật khẩu</label>
                            <input class="form-control" name="txtPassword" type="password" value="{!! old('txtPassword') !!}" id="password">
                        </div>
                        <div class="form-group">
                            <label class="control-label label-password">Nhập lại mật khẩu</label>
                            <input class="form-control" name="txtPassword_Confirm" type="password" value="{!! old('txtPassword_Confirm') !!}" id="password_confirm">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <select class="form-control select-status" id="status" name="txtStatus">
                                <option value="1">Hoạt động</option>
                                <option value="2">Khóa</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success my-btn-default btnAddPer" id="btnAddUser">
                        <i class="fa fa-save"></i>
                    </button>
                    <button type="button" class="btn btn-danger my-btn-default" data-dismiss="modal">
                        <i class="fa fa-times-circle"></i>Hủy bỏ
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal User -->

    <script>
        const urlGetUsers = '/admin/system/users/getUsers';
        const urlEdit = '/admin/system/users/edit';
        const urlList = '/admin/system/users/list';
        const urlDelete = '/admin/system/users/delete';
        const urlAdd = '/admin/system/users/add';

        //show dialog add
        $(".add-user").click(function(e) {
            e.preventDefault();
            //prosess form true is insert, false is update
            frmPerform(true);
        });

        //show dialog update
        $('#table tbody').on('click', '.update-users', function() {
            //prosess form true is insert, false is update
            self = this;
            frmPerform(false, self);
        });

        //load form true is insert, false is update
        function frmPerform(bln, self = null) {
            let txtTitle = "Thêm người dùng";
            $("input[name='txtFirstName']").val("");
            $("input[name='txtLastName']").val("");
            $("input[name='txtEmail']").val("");
            $("input[name='txtPassword']").val("");
            $("input[name='txtPassword_Confirm']").val("");
            $("input[name='txtStatus']").val("");
            $("input[name='txtId']").val("");
            $('.label-password').addClass("required");
            if(!bln) {
                txtTitle = "Sửa người dùng";
                let data = {
                    "id": $(self).data("id")
                }
                let result = ajaxQuery(urlGetUsers, data, 'POST');
                if(result.code==200) {
                    $("input[name='txtFirstName']").val(result.data.FirstName);
                    $("input[name='txtLastName']").val(result.data.LastName);
                    $("input[name='txtEmail']").val(result.data.email);
                    $("input[name='txtId']").val(result.data.id);
                    $(".select-status").val(result.data.status);
                    $('.label-password').removeClass("required");
                } else if(result.code==401) {
                    //noti
                    showAlert(result.message, result.notify, 5000);
                }

            }
            //edit title form
            $('#modal-users .modal-title strong').html(txtTitle);
            //edit button form
            $("#btnAddUser").contents().last()[0].textContent = txtTitle;
        }

        //add user
        $("#btnAddUser").click(function(e) {
            e.preventDefault();
            let _token = $('meta[name="csrf-token"]').attr('content');
            let txtId = $("input[name='txtId']").val();
            let txtFirstName = $("input[name='txtFirstName']").val();
            let txtLastName = $("input[name='txtLastName']").val();
            let txtEmail = $("input[name='txtEmail']").val();
            let txtPassword = $("input[name='txtPassword']").val();
            let txtPassword_Confirm = $("input[name='txtPassword_Confirm']").val();
            let txtStatus = $(".select-status").val();
            let data = {
                "_token": _token,
                id: txtId,
                FirstName: txtFirstName,
                LastName: txtLastName,
                Email: txtEmail,
                Password: txtPassword,
                Password_Confirm: txtPassword_Confirm,
                Status: txtStatus,
            };
            //query add permission
            let result = ajaxQuery(txtId=='' ? urlAdd : urlEdit, data, 'POST');
            if(result.code == 200) {
                //reload table
                reLoadTable();

                //close modal add
                $('#modal-users').modal('hide');
                $('.modal-backdrop').remove();

                //noti
                showAlert(result.message, result.notify, 5000);
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            } else {
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        });

        //prosess delete
        $(".btnDel").click(function(e) {
            var data = {
                "id": $(self).data("id"),
                "_token": $('meta[name="csrf-token"]').attr('content'),
            };
            //query del permission
            let result = ajaxQuery(urlDelete, data, 'POST');
            processResponse(result);
        });

        function processResponse(result) {
            if(result == undefined) {
                //noti
                showAlert("@Lang('global.msg_error')", "danger", 5000);
            }
            else if(result.code == 200) {
                //reload table
                reLoadTable();
                //noti
                showAlert(result.message, result.notify, 5000);

                //close modal add
                $('.modal').modal('hide');
                $('.modal-backdrop').remove();
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            }else{
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        }

        $(".filter-group").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                reLoadTable();
            }
        });

        $(document).on('click', '.pagination a', function(){
            let page = $(this).attr('href').split('page=')[1];
            reLoadTable(page);
            return false;
        });

        //reload table
        function reLoadTable(page = null) {
            let data = {
                "txt_search":  $('.filter-group').val()
            }
            let url = page==null ? urlList : urlList + '?page=' + page;

            let html = '';
            let arrResult = ajaxQuery(url, data, 'GET');
            if(arrResult.data.data.length > 0) {
                for(let i=0; i<arrResult.data.data.length; i++){
                    let stt = i + 1;
                    let item = arrResult.data.data[i];
                    let status = '<span class="label btn-primary-my label-many">Hoạt động</span>';
                    if(item.status != 1) status = '<span class="label btn-danger label-many">Khoá</span>';;
                    html +=
                        '<tr role="row" class="odd">' +
                        '<td class=" text-left no-sort">' +
                        '<div class="text-left">' +
                        '<div class="checkbox checkbox-primary table-checkbox">' +
                        '<input type="checkbox" class="checkboxes" name="id[]" value="1">' +
                        '</div>' +
                        '</div>' +
                        '</td>' +
                        '<td class="text-left column-key-stt sorting_1">' + stt + '</td>' +
                        '<td class="text-left column-key-name"><a class="title-name">' + item.name + '</a></td>' +
                        '<td class="column-key-role_name">' + item.email + '</td>' +
                        '<td class="column-key-role_name">' + status + '</td>' +
                        '<td class="column-key-role_name">' + formatDatetime(item.created_at) + '</td>' +
                        '<td class="column-key-role_name">' + formatDatetime(item.updated_at) + '</td>' +
                        '<td class=" text-right">' +
                        '<a href="" data-id="' + item.id + '" class="btn btn-primary-my update-users" data-toggle="modal" data-target="#modal-users"><i class="fa fa-edit"></i></a>' +
                        '<a href="" data-id="' + item.id + '" data-name="' + item.name + '" data-toggle="modal" data-target="#popup-delete" class="btn btn-danger deleteDialog"><i class="fa fa-trash"></i></a>' +
                        '</td>' +
                        '</tr>';
                }
                $("#table tbody").html(html);

                buildPagination(arrResult.data.current_page, arrResult.data.last_page, arrResult.data.total);
            } else {
                html += '<tr>' +
                    '<td colspan="9" class="text-center">' +
                    '<h2 class="no-result-grid">Không có dữ liệu</h2>' +
                    '</td></tr>';
                $("#table tbody").html(html);
            }
        }

        function buildPagination(current_page, last_page, total) {
            if(last_page > 1) {
                //remove class

                $('#table_paginate').remove();
                $('.datatables__info_wrap .clearfix').remove();

                //
                let html = '<div class="paging_simple_numbers pull-right" id="table_paginate"><ul class="pagination">';
                let num=1;
                if(current_page==1) {
                    html += '<li class="page-item disabled"><span class="page-link">«</span>';
                } else {
                    let num_pre = current_page - 1;
                    html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num_pre+'">«</a></li>';
                }

                for(let i=0; i<last_page; i++) {
                    if(num==current_page) {
                        html += '<li class="page-item active"><span class="page-link">'+num+'</span></li>';
                    } else {
                        html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num+'">'+num+'</a></li>';
                    }
                    num++;
                }

                if(current_page==last_page) {
                    html += '<li class="page-item disabled"><span class="page-link">»</span></li>';
                } else {
                    let num_next = current_page + 1;
                    html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num_next+'">»</a></li>';
                }

                html += '</ul>';
                $('.datatables__info_wrap').append(html);

                $('.datatables__info_wrap').append('<div class="clearfix"></div>');
            }

            //info tabel
            $('.datatables__info_wrap').children().remove();
            // $('#table_info').remove();
            let html = '<div class="dataTables_info pull-left" id="table_info"><span class="dt-length-records">Tổng ';
            html += total + ' đối tượng/ ' + last_page + ' trang</span></div>';
            $('.datatables__info_wrap').append(html);
            $('.datatables__info_wrap').append('<div class="clearfix"></div>');
        }

    </script>
@endsection

