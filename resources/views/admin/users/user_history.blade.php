<?php
use App\Models\Role;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Lịch sử người dùng')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active">Hện thống</li>
        <li class="breadcrumb-item active">Lịch sử người dùng</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
            </div>
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="dt-buttons btn-group">
                            <button class="btn btn-secondary buttons-reload" tabindex="0" aria-controls="table-users" onclick="reLoadTable()">
                                <span><i class="fas fa-sync"></i> Tải lại</span>
                            </button>
                        </div>
                        <table class="table table-striped table-hover vertical-middle dataTable no-footer" id="table" role="grid" aria-describedby="table-users_info" style="width: 1633px;">
                            <thead>
                            <tr role="row">
                                <th width="10px" class="text-left no-sort sorting_disabled" rowspan="1" colspan="1" style="width: 37px;" aria-label="">
                                    <input class="table-check-all" data-set=".dataTable .checkboxes" type="checkbox">
                                </th>
                                <th title="STT" class="text-left column-key-username sorting_desc" style="width: 50px;">STT</th>
                                <th title="Name" class="text-left column-key-email sorting" style="width: 250px;">Tên</th>
                                <th title="Email" width="100px" class="column-key-created_at sorting" style="width: 100px;">Hoạt động</th>
                                <th title="Email" width="100px" class="column-key-created_at sorting" style="width: 100px;">Chú ý</th>
                                <th title="Created_at" width="100px" class="column-key-status sorting" style="width: 100px;">Thời gian tạo</th>
                                <th title="Update_at" width="100px" class="column-key-status sorting" style="width: 100px;">Thời gian sửa</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data) && count($data)>0)
                                @foreach($data as $key => $item)
                                    <tr role="row" class="odd">
                                        <td class=" text-left no-sort">
                                            <div class="text-left">
                                                <div class="checkbox checkbox-primary table-checkbox">
                                                    <input type="checkbox" class="checkboxes" name="id[]" value="1">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-left column-key-username sorting_1">
                                            {{ ++$key }}
                                        </td>
                                        <td class="text-left column-key-email">
                                            <a class="title-email">{{ $item->name }}</a>
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->action }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->note }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->created_at }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->updated_at }}
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="datatables__info_wrap">
                            <div class="dataTables_info pull-left" id="table_info">
                                <span class="dt-length-records">
                                    Tổng {{ $data->total() }} đối tượng/ {{ $data->lastPage() }} trang
                                </span>
                            </div>
                            <div class="paging_simple_numbers pull-right" id="table_paginate">
                                {!! $data->links() !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        const urlList = '/admin/system/users/user_history';

        //reload table
        function reLoadTable(page = null) {
            let data = {
                "txt_search":  $('.filter-group').val()
            }
            let url = page==null ? urlList : urlList + '?page=' + page;
            let html = '';


            let arrResult = ajaxQuery(url, data, 'GET');
            if(arrResult.data.data.length > 0) {

                for(let i=0; i<arrResult.data.data.length; i++){
                    let stt = i + 1;
                    let item = arrResult.data.data[i];
                    html +=
                        '<tr role="row" class="odd">' +
                        '<td class=" text-left no-sort">' +
                        '<div class="text-left">' +
                        '<div class="checkbox checkbox-primary table-checkbox">' +
                        '<input type="checkbox" class="checkboxes" name="id[]" value="1">' +
                        '</div>' +
                        '</div>' +
                        '</td>' +
                        '<td class="text-left column-key-stt sorting_1">' + stt + '</td>' +
                        '<td class="text-left column-key-name"><a class="title-name">' + item.name + '</a></td>' +
                        '<td class="text-left column-key-name"><a class="title-name">' + item.action + '</a></td>' +
                        '<td class="column-key-role_name">' + item.note + '</td>' +
                        '<td class="column-key-role_name">' + formatDatetime(item.created_at) + '</td>' +
                        '<td class="column-key-role_name">' + formatDatetime(item.updated_at) + '</td>' +
                        '</tr>';
                }
                $("#table tbody").html(html);
            } else {
                html += '<tr>' +
                    '<td colspan="8" class="text-center">' +
                    '<h2 class="no-result-grid">Không có dữ liệu</h2>' +
                    '</td></tr>';
                $("#table tbody").html(html);
            }
            buildPagination(arrResult.data.current_page, arrResult.data.last_page, arrResult.data.total);
        }

        function buildPagination(current_page, last_page, total) {
            //remove class
            $('#table_paginate').remove();

            if(last_page > 1) {
                $('.datatables__info_wrap .clearfix').remove();

                //
                let html = '<div class="paging_simple_numbers pull-right" id="table_paginate"><ul class="pagination">';
                let num=1;
                if(current_page==1) {
                    html += '<li class="page-item disabled"><span class="page-link">«</span>';
                } else {
                    let num_pre = current_page - 1;
                    html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num_pre+'">«</a></li>';
                }

                for(let i=0; i<last_page; i++) {
                    if(num==current_page) {
                        html += '<li class="page-item active"><span class="page-link">'+num+'</span></li>';
                    } else {
                        html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num+'">'+num+'</a></li>';
                    }
                    num++;
                }

                if(current_page==last_page) {
                    html += '<li class="page-item disabled"><span class="page-link">»</span></li>';
                } else {
                    let num_next = current_page + 1;
                    html += '<li class="page-item"><a class="page-link" href="'+urlList+'?page='+num_next+'">»</a></li>';
                }

                html += '</ul>';
                $('.datatables__info_wrap').append(html);


            }

            //info tabel
            $('#table_info').remove();
            let html = '<div class="dataTables_info pull-left" id="table_info"><span class="dt-length-records">Tổng ';
            html += total + ' đối tượng/ ' + last_page + ' trang</span></div>';
            $('.datatables__info_wrap').append(html);
            $('.datatables__info_wrap').append('<div class="clearfix"></div>');
        }
    </script>
@endsection

