@extends('admin.layouts.index')
@section('pageTitle', 'Dashboard')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="clearfix"></div>
    <div class="note note-success">
        <p>Hi {{ Auth::user()->name }}!</p>
    </div>
@endsection
