<?php
use App\Models\Accused;use App\Models\Coroner;use App\Models\Criminal;use App\Models\Role;use App\Models\Surveyor;
?>
@extends('admin.layouts.index')
@section('pageTitle', 'Tin báo, vụ án')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active">Platform Administration</li>
        <li class="breadcrumb-item active">Tin báo, vụ án</li>
    </ol>
    <div class="clearfix"></div>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
            </div>
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div id="table-users_filter" class="dataTables_filter">
                            <label>
                                <input type="search" class="form-control form-control-sm filter-group" placeholder="Search..." aria-controls="table-users">
                            </label>
                        </div>
                        <div class="dt-buttons btn-group">
                            <button class="btn btn-secondary action-item add-news" tabindex="0" aria-controls="table" data-toggle="modal" data-target="#modal-news">
                                <a href="#">
                                    <i class="fa fa-plus"></i> Create
                                </a>
                            </button>

                            <button class="btn btn-secondary buttons-reload" tabindex="0" aria-controls="table-users" onclick="reLoadTable()">
                                <span><i class="fas fa-sync"></i> Reload</span>
                            </button>
                        </div>
                        <table class="table table-striped table-hover vertical-middle dataTable no-footer" id="table" role="grid" aria-describedby="table-users_info" style="width: 2750px !important;">
                            <thead>
                                <tr role="row">
                                    <th title="STT" width="50px" rowspan="2" class="text-left column-key-username sorting_desc">STT</th>
                                    <th title="Name" width="300px" rowspan="2" class="text-left column-key-email sorting">Nội dung tin báo</th>
                                    <th title="Email" width="300px" rowspan="2" width="100px" class="column-key-created_at sorting">Nội dung vụ án</th>
                                    <th title="Status" rowspan="2" width="200px" class="column-key-created_at sorting">Điều tra viên</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Khảo sát viên</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Tội danh</th>
                                    <th title="Created_at" rowspan="2" width="100px" class="column-key-status sorting">Kết quả điều tra</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Số, ngày phân công tin báo</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Tạm đình chỉ tin báo</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Số, ngày KTVA</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Tạm đình chỉ vụ án</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Khỏi tố</th>
                                    <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Bị can</th>
                                    <th colspan="5" class="text-center" >Biện pháp ngăn chặn</th>
                                </tr>
                                <tr>
                                    <th width="50px">Bắt tạm giam</th>
                                    <th width="50px">Tạm giam</th>
                                    <th width="50px">Cấm</th>
                                    <th width="50px">Bảo lãnh</th>
                                    <th width="50px">BPNC KHÁC</th>
                                </tr>
                            </thead>
                            <tbody>
                            <form method="GET">
                            <tr>
                                <td></td>
                                <td><input class="form-control noidungtinbao" type="text" name="noidungtinbao" value="{{ isset($_GET['noidungtinbao'])&&$_GET['noidungtinbao']!="" ? $_GET['noidungtinbao'] : ''}}"></td>
                                <td><input class="form-control noidungvuan" type="text" value="{{ isset($_GET['noidungvuan'])&&$_GET['noidungvuan']!="" ? $_GET['noidungvuan'] : ''}}"></td>
                                <td>
                                    <select class="form-control select-coroner select2">
                                        <option value="0">{{ 'Chọn điều tra viên' }}</option>
                                        <?php $coroner = Coroner::getAllCoroner();?>
                                        @if(!is_null($coroner))
                                            @foreach($coroner as $co)
                                                <option value="{{ $co->id }}" {{ isset($_GET['dtv'])&&$_GET['dtv']==$co->id ? 'selected' : ''}}>{{ $co->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select-surveyor select2">
                                        <option value="0">{{ 'Chọn khảo sát viên' }}</option>
                                        <?php $surveyor = Surveyor::getAllSurveyor();?>
                                        @if(!is_null($surveyor))
                                            @foreach($surveyor as $co)
                                                <option value="{{ $co->id }}">{{ $co->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select-criminal select2">
                                        <option value="0">{{ 'Chọn tội danh' }}</option>
                                        <?php $criminal = Criminal::getAllCriminal();?>
                                        @if(!is_null($criminal))
                                            @foreach($criminal as $co)
                                                <option value="{{ $co->id }}">{{ $co->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td><input class="form-control ketquadieutra" type="text" value="{{ isset($_GET['ketquadieutra'])&&$_GET['ketquadieutra']!="" ? $_GET['ketquadieutra'] : ''}}"></td>
                                <td></td>
                                <td>
                                    <select class="form-control select-tamdinhchibaotin">
                                        <option value="" {{ isset($_GET['tamdinhchibaotin'])&&$_GET['tamdinhchibaotin']=="" ? 'selected' : ''}}>Chọn hoạt động</option>
                                        <option value="0" {{ isset($_GET['tamdinhchibaotin'])&&$_GET['tamdinhchibaotin']=="0" ? 'selected' : ''}}>Hoạt động bình thường</option>
                                        <option value="1" {{ isset($_GET['tamdinhchibaotin'])&&$_GET['tamdinhchibaotin']=="1" ? 'selected' : ''}}>Đình chỉ báo tin</option>
                                    </select>
                                </td>
                                <td></td>
                                <td>
                                    <select class="form-control select-tamdinhchivuan">
                                        <option value="" {{ isset($_GET['tamdinhchivuan'])&&$_GET['tamdinhchivuan']=="" ? 'selected' : ''}}>Chọn hoạt động</option>
                                        <option value="0" {{ isset($_GET['tamdinhchivuan'])&&$_GET['tamdinhchivuan']=="0" ? 'selected' : ''}}>Không đình chỉ</option>
                                        <option value="1" {{ isset($_GET['tamdinhchivuan'])&&$_GET['tamdinhchivuan']=="1" ? 'selected' : ''}}>Đình chỉ vụ án</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control select-khoito">
                                        <option value="" {{ isset($_GET['khoito'])&&$_GET['khoito']=="" ? 'selected' : ''}}>Chọn hoạt động</option>
                                        <option value="0" {{ isset($_GET['khoito'])&&$_GET['khoito']=="0" ? 'selected' : ''}}>Không khởi tố</option>
                                        <option value="1" {{ isset($_GET['khoito'])&&$_GET['khoito']=="1" ? 'selected' : ''}}>Khởi tố</option>
                                    </select>
                                </td>
                                <td>
{{--                                    <select class="form-control select-accused select2">--}}
{{--                                        <?php $accused = Accused::getAllAccused();?>--}}
{{--                                        @if(!is_null($accused))--}}
{{--                                            @foreach($accused as $acc)--}}
{{--                                                <option value="{{ $acc->id }}">{{ $acc->name }}</option>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                    </select>--}}
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </form>
                            @if(isset($news))
                            @foreach($news as $key => $item)
                                <tr role="row" class="odd">
                                    <td class="text-left column-key-username sorting_1">
                                        {{ ++$key }}
                                    </td>
                                    <td class="text-left column-key-email">
                                        <a class="title-email">{{ $item->noidungtinbao }}</a>
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->noidungvuan }}
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->parentCoronel!=null ? $item->parentCoronel->name : "" }}
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->parentSurveyor!=null ? $item->parentSurveyor->name : "" }}
                                    </td>
                                    <td class="column-key-role_name">
                                        {{ $item->parentCriminal!=null ? $item->parentCriminal->name : "" }}
                                    </td>
                                    <td class="column-key-role_name">
                                        {!! $item->ketquadieutra !!}
                                    </td>
                                    <td class="column-key-role_name">
                                        <?php $ngBD = is_null($item->ngaybatdautinbao) ? "" : date('d/m/Y', strtotime($item->ngaybatdautinbao));
                                              $ngKT = is_null($item->ngayketthuctinbao) ? "" : date('d/m/Y', strtotime($item->ngayketthuctinbao));
                                            $giahan = "";
                                            if(!is_null($item->ngaybatdaugiahantinbao) || !is_null($item->ngaykethucgiahantinbao)) {
                                                $ngBDGH = is_null($item->ngaybatdaugiahantinbao) ? "" : date('d/m/Y', strtotime($item->ngaybatdaugiahantinbao));
                                                $ngKTGH = is_null($item->ngaykethucgiahantinbao) ? "" : date('d/m/Y', strtotime($item->ngaykethucgiahantinbao));
                                                $giahan = " Gia hạn ".$ngBDGH.' - '.$ngKTGH;
                                            }
                                        ?>
                                        {{ 'Số '.$item->sotinbao.' ngày '.$ngBD.' - '.$ngKT."\n".$giahan}}
                                    </td>
                                    <td class="column-key-role_name">
                                        @if($item->tamdinhchibaotin==0)
                                            {{ 'Hoạt động bình thường' }}
                                        @else
                                            {{ 'Đình chỉ báo tin' }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $ngBD = is_null($item->ngaybatdauktva) ? "" : date('d/m/Y', strtotime($item->ngaybatdauktva));
                                              $ngKT = is_null($item->ngaykethucktva) ? "" : date('d/m/Y', strtotime($item->ngaykethucktva));

                                        ?>
                                        {{ 'Số '.$item->soktva.' ngày '.$ngBD.' - '.$ngKT}}
                                    </td>
                                    <td>
                                        @if($item->tamdinhchivuan==0)
                                            {{ 'Không đình chỉ' }}
                                        @else
                                            {{ 'Đình chỉ vụ án' }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->khoito==0)
                                            {{ 'Không khởi tố' }}
                                        @else
                                            {{ 'Khởi tố' }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                        $bican = [];
                                        $lstBican = Accused::findAccused($item->id);

                                        if(!is_null($lstBican)) {
                                            foreach ($lstBican as $val) {
                                                array_push($bican, $val->name);
                                            }
                                        }
                                        ?>
                                        {!! implode("<br>", $bican) !!}
                                    </td>
                                    <td>{{ $item->bienphapnganchan==1 ? 'x' : '' }}</td>
                                    <td>{{ $item->bienphapnganchan==2 ? 'x' : '' }}</td>
                                    <td>{{ $item->bienphapnganchan==3 ? 'x' : '' }}</td>
                                    <td>{{ $item->bienphapnganchan==4 ? 'x' : '' }}</td>
                                    <td>{{ $item->bienphapnganchan==5 ? 'x' : '' }}</td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="datatables__info_wrap">
                            <div class="dataTables_info pull-left" id="table_info">
                                <span class="dt-length-records">
                                    Tổng {{ $news->total() }} đối tượng/ {{ $news->lastPage() }} trang
                                </span>
                            </div>
                            <div class="paging_simple_numbers pull-right" id="table_paginate">
                                @if(isset($_GET['noidungtinbao']))
                                    {!! $news->appends(['noidungtinbao'=>$_GET['noidungtinbao'],
                                                        'noidungvuan'=>$_GET['noidungvuan'],
                                                        'ketquadieutra'=>$_GET['ketquadieutra'],
                                                        'dtv'=>$_GET['dtv'],
                                                        'ksv'=>$_GET['ksv'],
                                                        'td'=>$_GET['td'],
                                                        'tamdinhchibaotin'=>$_GET['tamdinhchibaotin'],
                                                        'tamdinhchivuan'=>$_GET['tamdinhchivuan'],
                                                        'khoito'=>$_GET['khoito']])->render() !!}
                                @else
                                    {!! $news->render() !!}
                                @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        table {
            display: block;
            overflow-x: auto;
            white-space: nowrap;
        }
        .page-content .dataTables_wrapper td, .page-content .dataTables_wrapper th {
            white-space: normal;
        }
        .table-responsive {
            overflow-x: auto !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 15px;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }

    </style>

    <script>
        $(".select2").select2();
        $(".select2").val("").trigger("change");
        $(".select-coroner").val(getParameterByName('dtv')).trigger("change");
        $(".select-surveyor").val(getParameterByName('ksv')).trigger("change");
        $(".select-criminal").val(getParameterByName('td')).trigger("change");

        $(".noidungvuan").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                window.location = urlSearch();
            }
        });
        $(".noidungtinbao").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                window.location = urlSearch();
            }
        });
        $(".ketquadieutra").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                window.location = urlSearch();
            }
        });

        $(document.body).on("change",".select-coroner",function(){
            window.location = urlSearch();
        });

        $(document.body).on("change",".select-surveyor",function(){
            window.location = urlSearch();
        });

        $(document.body).on("change",".select-criminal",function(){
            window.location = urlSearch();
        });
        $(document.body).on("change",".select-tamdinhchibaotin",function(){
            window.location = urlSearch();
        });
        $(document.body).on("change",".select-tamdinhchivuan",function(){
            window.location = urlSearch();
        });
        $(document.body).on("change",".select-khoito",function(){
            window.location = urlSearch();
        });

        function urlSearch() {
            let noidungtinbao = 'noidungtinbao=' + $('.noidungtinbao').val();
            let noidungvuan = '&noidungvuan=' + $('.noidungvuan').val();
            let ketquadieutra = '&ketquadieutra=' + $('.ketquadieutra').val();
            let dtv = '&dtv=' + $('.select-coroner').val();
            let ksv = '&ksv=' + $('.select-surveyor').val();
            let td = '&td=' + $('.select-criminal').val();
            let tamdinhchibaotin = '&tamdinhchibaotin=' + $('.select-tamdinhchibaotin').val();
            let tamdinhchivuan = '&tamdinhchivuan=' + $('.select-tamdinhchivuan').val();
            let khoito = '&khoito=' + $('.select-khoito').val();

            return '/admin/report/news?'+noidungtinbao+noidungvuan+ketquadieutra+dtv+ksv+td+tamdinhchibaotin+tamdinhchivuan+khoito;
        }

        function getParameterByName(name, url = window.location.href) {
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

    </script>
@endsection

