<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.partials.head')
</head>
<body class="page-sidebar-closed-hide-logo page-content-white page-container-bg-solid" style="">
<div class="page-wrapper">
    @include('admin.partials.topbar')
    <div class="clearfix"></div>
    <!-- Page container -->
    <div class="page-container col-md-12" style="overflow: hidden">
        @include('admin.partials.sidebar')

        <div class="page-content-wrapper">
            <div class="page-content ">
                <div id="app">
                    <flash-component message=""></flash-component>
                    @yield('content')
                </div>
                <div class="page-footer">
                    <div class="page-footer-inner">
                        <div class="row">
                            <div class="col-md-6">
                                Copyright 2021 &copy;
                            </div>
                            <div class="col-md-6 text-right">
                                Page loaded in 0.1s
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('admin.partials.javascripts')
@stack('custom-scripts')
</body>
</html>
