<!DOCTYPE html>
<html>
    <head>
        <title>MUSIC THEATRE</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">

        <!-- plugin css -->
        {!! Html::style('admin/assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
        {!! Html::style('admin/assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
        <!-- end plugin css -->
            <!-- plugin css -->
        @stack('plugin-styles')
        <!-- end plugin css -->

        <!-- common css -->
        {!! Html::style('admin/css/app2.css') !!}
        {!! Html::style('admin/css/my-style.css') !!}
        <!-- end common css -->

        @stack('style')
    </head>
    <body data-base-url="{{url('/')}}">

        <div class="container-scroller" id="app">
            <div class="container-fluid page-body-wrapper full-page-wrapper">
                @yield('content')
            </div>
        </div>
        <!-- base js -->
        {!! Html::script('admin/js/app2.js') !!}

        <!-- end base js -->

        <!-- plugin js -->
        @stack('plugin-scripts')
        <!-- end plugin js -->
        {!! Html::script('admin/js/my-ajax.js') !!}
        {!! Html::script('admin/js/myJs.js') !!}
        @stack('custom-scripts')
    </body>
</html>
