<?php
use App\Models\Accused;
use App\Models\Coroner;
use App\Models\Criminal;
use App\Models\Role;
use App\Models\Surveyor;


?>
@extends('admin.layouts.index')
@section('pageTitle', 'Danh sách tin báo, vụ án')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
        <li class="breadcrumb-item active">Quản lý tin báo, vụ án</li>
        <li class="breadcrumb-item active">Danh sách tin báo, vụ án</li>
    </ol>
    <div class="table-wrapper">
        <div class="portlet light bordered portlet-no-padding">
            <div class="portlet-title">
                <div class="dt-buttons btn-group">
                    <button class="btn btn-secondary action-item add-news" tabindex="0" aria-controls="table" data-toggle="modal" data-target="#modal-news">
                        <a href="#">
                            <i class="fa fa-plus"></i> Thêm mới
                        </a>
                    </button>
                    <a href="/admin/news/export" class="btn btn-secondary buttons-reload" tabindex="0" aria-controls="table-users">
                        <span><i class="fas fa-download"></i> Excel</span>
                    </a>
                    <a href="/admin/news/list" class="btn btn-secondary buttons-reload" tabindex="0" aria-controls="table-users">
                        <span><i class="fas fa-sync"></i> Tải lại</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive table-has-actions table-has-filter ">
                    <div id="table-users_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                        <table class="table table-striped table-hover vertical-middle dataTable no-footer" id="table" role="grid" aria-describedby="table-users_info" style="width: 2750px !important;">
                            <thead>
                            <tr role="row">
                                <th title="STT" width="50px" rowspan="2" class="text-left column-key-username sorting_desc">STT</th>
                                <th title="Name" width="300px" rowspan="2" class="text-left column-key-email sorting">Nội dung tin báo</th>
                                <th title="Email" width="300px" rowspan="2" width="100px" class="column-key-created_at sorting">Nội dung vụ án</th>
                                <th title="Status" rowspan="2" width="200px" class="column-key-created_at sorting">Điều tra viên</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Kiểm sát viên</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Tội danh</th>
                                <th title="Created_at" rowspan="2" width="100px" class="column-key-status sorting">Kết quả điều tra</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Số, ngày phân công tin báo</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Tạm đình chỉ tin báo</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Số, ngày KTVA</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Tạm đình chỉ vụ án</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Khỏi tố</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Bị can</th>
                                <th title="Created_at" rowspan="2" width="200px" class="column-key-status sorting">Ngày hết hạn điều tra</th>
                                <th colspan="5" class="text-center" >Biện pháp ngăn chặn</th>
                                <th rowspan="2"></th>
                            </tr>
                            <tr>
                                <th width="50px">Bắt tạm giam</th>
                                <th width="50px">Tạm giam</th>
                                <th width="50px">Cấm</th>
                                <th width="50px">Bảo lãnh</th>
                                <th width="50px">BPNC KHÁC</th>
                            </tr>
                            </thead>
                            <tbody>
                            <form method="GET">
                                <tr>
                                    <td></td>
                                    <td><input class="form-control noidungtinbao" type="text" name="noidungtinbao" value="{{ isset($_GET['noidungtinbao'])&&$_GET['noidungtinbao']!="" ? $_GET['noidungtinbao'] : ''}}"></td>
                                    <td><input class="form-control noidungvuan" type="text" value="{{ isset($_GET['noidungvuan'])&&$_GET['noidungvuan']!="" ? $_GET['noidungvuan'] : ''}}"></td>
                                    <td>
                                        <select class="form-control search-coroner select2">
                                            <option value="0">{{ 'Chọn điều tra viên' }}</option>
                                            <?php $coroner = Coroner::getAllCoroner();?>
                                            @if(!is_null($coroner))
                                                @foreach($coroner as $co)
                                                    <option value="{{ $co->id }}" {{ isset($_GET['dtv'])&&$_GET['dtv']==$co->id ? 'selected' : ''}}>{{ $co->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control search-surveyor select2">
                                            <option value="0">{{ 'Chọn khảo sát viên' }}</option>
                                            <?php $surveyor = Surveyor::getAllSurveyor();?>
                                            @if(!is_null($surveyor))
                                                @foreach($surveyor as $co)
                                                    <option value="{{ $co->id }}">{{ $co->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control search-criminal select2">
                                            <option value="0">{{ 'Chọn tội danh' }}</option>
                                            <?php $criminal = Criminal::getAllCriminal();?>
                                            @if(!is_null($criminal))
                                                @foreach($criminal as $co)
                                                    <option value="{{ $co->id }}">{{ $co->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td><input class="form-control ketquadieutra" type="text" value="{{ isset($_GET['ketquadieutra'])&&$_GET['ketquadieutra']!="" ? $_GET['ketquadieutra'] : ''}}"></td>
                                    <td></td>
                                    <td>
                                        <select class="form-control search-tamdinhchibaotin">
                                            <option value="" {{ isset($_GET['tamdinhchibaotin'])&&$_GET['tamdinhchibaotin']=="" ? 'selected' : ''}}>Chọn hoạt động</option>
                                            <option value="0" {{ isset($_GET['tamdinhchibaotin'])&&$_GET['tamdinhchibaotin']=="0" ? 'selected' : ''}}>Hoạt động bình thường</option>
                                            <option value="1" {{ isset($_GET['tamdinhchibaotin'])&&$_GET['tamdinhchibaotin']=="1" ? 'selected' : ''}}>Đình chỉ báo tin</option>
                                        </select>
                                    </td>
                                    <td></td>
                                    <td>
                                        <select class="form-control search-tamdinhchivuan">
                                            <option value="" {{ isset($_GET['tamdinhchivuan'])&&$_GET['tamdinhchivuan']=="" ? 'selected' : ''}}>Chọn hoạt động</option>
                                            <option value="0" {{ isset($_GET['tamdinhchivuan'])&&$_GET['tamdinhchivuan']=="0" ? 'selected' : ''}}>Không đình chỉ</option>
                                            <option value="1" {{ isset($_GET['tamdinhchivuan'])&&$_GET['tamdinhchivuan']=="1" ? 'selected' : ''}}>Đình chỉ vụ án</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control search-khoito">
                                            <option value="" {{ isset($_GET['khoito'])&&$_GET['khoito']=="" ? 'selected' : ''}}>Chọn hoạt động</option>
                                            <option value="0" {{ isset($_GET['khoito'])&&$_GET['khoito']=="0" ? 'selected' : ''}}>Không khởi tố</option>
                                            <option value="1" {{ isset($_GET['khoito'])&&$_GET['khoito']=="1" ? 'selected' : ''}}>Khởi tố</option>
                                        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </form>
                            @if(isset($news))
                                @foreach($news as $key => $item)
                                    <tr role="row" class="odd">
                                        <td class="text-left column-key-username sorting_1">
                                            {{ ++$key }}
                                        </td>
                                        <td class="text-left column-key-email">
                                            <a class="title-email">{{ $item->noidungtinbao }}</a>
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->noidungvuan }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->parentCoronel!=null ? $item->parentCoronel->name : "" }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->parentSurveyor!=null ? $item->parentSurveyor->name : "" }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {{ $item->parentCriminal!=null ? $item->parentCriminal->name : "" }}
                                        </td>
                                        <td class="column-key-role_name">
                                            {!! $item->ketquadieutra !!}
                                        </td>
                                        <td class="column-key-role_name">
                                            <?php $ngBD = is_null($item->ngaybatdautinbao) ? "" : date('d/m/Y', strtotime($item->ngaybatdautinbao));
                                            $ngKT = is_null($item->ngayketthuctinbao) ? "" : date('d/m/Y', strtotime($item->ngayketthuctinbao));
                                            $giahan = "";
                                            if(!is_null($item->ngaybatdaugiahantinbao) || !is_null($item->ngaykethucgiahantinbao)) {
                                                $ngBDGH = is_null($item->ngaybatdaugiahantinbao) ? "" : date('d/m/Y', strtotime($item->ngaybatdaugiahantinbao));
                                                $ngKTGH = is_null($item->ngaykethucgiahantinbao) ? "" : date('d/m/Y', strtotime($item->ngaykethucgiahantinbao));
                                                $giahan = " Gia hạn ".$ngBDGH.' - '.$ngKTGH;
                                            }
                                            ?>
                                            {{ 'Số '.$item->sotinbao.' ngày '.$ngBD.' - '.$ngKT."\n".$giahan}}
                                        </td>
                                        <td class="column-key-role_name">
                                            @if($item->tamdinhchibaotin==0)
                                                {{ 'Hoạt động bình thường' }}
                                            @else
                                                {{ 'Đình chỉ báo tin' }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $ngBD = is_null($item->ngaybatdauktva) ? "" : date('d/m/Y', strtotime($item->ngaybatdauktva));
                                            $ngKT = is_null($item->ngaykethucktva) ? "" : date('d/m/Y', strtotime($item->ngaykethucktva));

                                            ?>
                                            {{ 'Số '.$item->soktva.' ngày '.$ngBD.' - '.$ngKT}}
                                        </td>
                                        <td>
                                            @if($item->tamdinhchivuan==0)
                                                {{ 'Không đình chỉ' }}
                                            @else
                                                {{ 'Đình chỉ vụ án' }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->khoito==0)
                                                {{ 'Không khởi tố' }}
                                            @else
                                                {{ 'Khởi tố' }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php
                                            $bican = [];
                                            $lstBican = Accused::findAccused($item->id);

                                            if(!is_null($lstBican)) {
                                                foreach ($lstBican as $val) {
                                                    array_push($bican, $val->name);
                                                }
                                            }
                                            ?>
                                            {!! implode("<br>", $bican) !!}
                                        </td>
                                        <td>{{ !is_null($item->handieutra) ? date('d/m/Y', strtotime($item->hantamgiam)) : "" }}</td>
                                        <td>{{ $item->bienphapnganchan==1 ? !is_null($item->hantamgiam) ? date('d/m/Y', strtotime($item->hantamgiam)) : "" : "" }}</td>
                                        <td>{{ $item->bienphapnganchan==2 ? !is_null($item->hantamgiam) ? date('d/m/Y', strtotime($item->hantamgiam)) : "" : "" }}</td>
                                        <td>{{ $item->bienphapnganchan==3 ? 'x' : '' }}</td>
                                        <td>{{ $item->bienphapnganchan==4 ? 'x' : '' }}</td>
                                        <td>{{ $item->bienphapnganchan==5 ? 'x' : '' }}</td>
                                        <td class=" text-right">
                                            <a href="" data-id="{{ $item->id }}" class="btn btn-primary-my update-news" data-toggle="modal" data-target="#modal-news"><i class="fa fa-edit"></i></a>
                                            <a href="" data-id="{{ $item->id }}" data-name="{{ $item->noidungtinbao }}" data-toggle="modal" data-target="#popup-delete" class="btn btn-icon btn-danger deleteDialog"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="datatables__info_wrap">
                    <div class="dataTables_info pull-left" id="table_info">
                                <span class="dt-length-records">
                                    Tổng {{ $news->total() }} đối tượng/ {{ $news->lastPage() }} trang
                                </span>
                    </div>
                    <div class="paging_simple_numbers pull-right" id="table_paginate">
                        @if(isset($_GET['noidungtinbao']))
                            {!! $news->appends(['noidungtinbao'=>$_GET['noidungtinbao'],
                                                'noidungvuan'=>$_GET['noidungvuan'],
                                                'ketquadieutra'=>$_GET['ketquadieutra'],
                                                'dtv'=>$_GET['dtv'],
                                                'ksv'=>$_GET['ksv'],
                                                'td'=>$_GET['td'],
                                                'tamdinhchibaotin'=>$_GET['tamdinhchibaotin'],
                                                'tamdinhchivuan'=>$_GET['tamdinhchivuan'],
                                                'khoito'=>$_GET['khoito']])->render() !!}
                        @else
                            {!! $news->render() !!}
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.global.popupDelete')

    <!-- Modal news -->
    <div id="modal-news" class="modal fade" role="dialog">
        <div class="modal-dialog width-modal">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-modal my-modal">
                    <h4 class="modal-title">
                        <i class="til_img"></i>
                        <strong>Sửa điều tra viên</strong>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="txtId">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="main-form">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label required">Nội dung tin báo</label>
                                        <input class="form-control" name="txtNoidngtinbao" type="text" value="{!! old('txtNoidngtinbao') !!}" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label required">Nội dung vụ án</label>
                                        <input class="form-control" name="txtNoidungvuan" type="text" value="{!! old('txtNoidungvuan') !!}" id="phone">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-labe">Kết quả điều tra</label>
                                        <textarea class="form-control txtKetquadieutra" name="txtKetquadieutra" type="text"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Số tin báo</label>
                                        <input class="form-control" name="txtSotinbao" type="text">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày bắt đầu tin báo</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="txtNgaybatdautinbao" type="text">
                                                    <span class="input-group-prepend">
                                                        <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày kết thúc tin báo</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="txtNgayketthuctinbao" type="text">
                                                    <span class="input-group-prepend">
                                                        <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row giahan hidden">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày bắt đầu gia hạn tin báo</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="txtNgaybatdaugiahan" type="text">
                                                    <span class="input-group-prepend">
                                                        <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày kết thúc gia hạn tin báo</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="txtNgayketthucgiahan" type="text">
                                                    <span class="input-group-prepend">
                                                        <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Số KTVA</label>
                                        <input class="form-control" name="txtSoKTVA" type="text">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày bắt đầu KTVA</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="txtNgaybatdauktva" type="text">
                                                    <span class="input-group-prepend">
                                                        <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ngày kết thúc KTVA</label>
                                                <div class="input-group">
                                                    <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="txtNgaykethucktva" type="text">
                                                    <span class="input-group-prepend">
                                                        <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="main-form">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">Điều tra viên</label>
                                        <select class="form-control select-coroner select2">
                                            <?php $coroner = Coroner::getAllCoroner();?>
                                            @if(!is_null($coroner))
                                                @foreach($coroner as $co)
                                                    <option value="{{ $co->id }}">{{ $co->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Kiểm sát viên</label>
                                        <select class="form-control select-surveyor select2">
                                            <?php $surveyor = Surveyor::getAllSurveyor();?>
                                            @if(!is_null($surveyor))
                                                @foreach($surveyor as $co)
                                                    <option value="{{ $co->id }}">{{ $co->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Loại tội phạm</label>
                                        <select class="form-control select-criminal select2">
                                            <?php $criminal = Criminal::getAllCriminal();?>
                                            @if(!is_null($criminal))
                                                @foreach($criminal as $co)
                                                    <option value="{{ $co->id }}">{{ $co->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tạm đình chỉ báo tin</label>
                                        <select class="form-control select-tamdinhchibaotin">
                                            <option value="0">Hoạt động bình thường</option>
                                            <option value="1">Đình chỉ báo tin</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tạm đình chỉ vụ án</label>
                                        <select class="form-control select-tamdinhchivuan">
                                            <option value="0">Không đình chỉ</option>
                                            <option value="1">Đình chỉ vụ án</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Khởi tố</label>
                                        <select class="form-control select-khoito">
                                            <option value="0">Không khởi tố</option>
                                            <option value="1">Khởi tố</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Biện pháp ngăn chặn</label>
                                        <select class="form-control select-bienphamnganchan">
                                            <option value="0">Chưa có biện pháp</option>
                                            <option value="1">Bắt tạm giam</option>
                                            <option value="2">Tạm giam</option>
                                            <option value="3">Cấm</option>
                                            <option value="4">Bảo lãnh</option>
                                            <option value="5">BPNC khác</option>
                                        </select>
                                    </div>
                                    <div class="form-group form-hantamgiam hidden">
                                        <label class="control-label">Ngày hết hạn tạm giam</label>
                                        <div class="input-group">
                                            <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="hantamgiam" type="text">
                                            <span class="input-group-prepend">
                                                <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group remove-bican">
                                        <label class="control-label">Bị can</label>
                                        <select class="form-control select-bican" name="bican[]" multiple="multiple">
{{--                                            <option value="AL">Alabama</option>--}}
{{--                                            <option value="WY">Wyoming</option>--}}
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ngày hết hạn điều tra</label>
                                        <div class="input-group">
                                            <input class="form-control datepicker" data-date-format="yyyy-mm-dd" name="handieutra" type="text">
                                            <span class="input-group-prepend">
                                                <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success my-btn-default btnAddPer" id="btnAddNews">
                        <i class="fa fa-save"></i>
                    </button>
                    <button type="button" class="btn btn-danger my-btn-default" data-dismiss="modal">
                        <i class="fa fa-times-circle"></i>Hủy bỏ
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal news -->

    <style>
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 15px;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }

        .input-group .input-group-prepend {
            padding: initial;
        }

        .table-condensed tr th {
            background-color: initial !important;
            color: initial !important;
            text-align: center !important;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-selection__choice {
            line-height: 1.7;
            margin-top: 5px;
            border: 1px solid #aaa;
        }
        .select2-container .select2-selection--multiple {
            min-height: 43px;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search__field {
            line-height: 32px;
            margin-top: 5px;
        }
        .width-modal {
            width: 1280px;
            max-width: initial;
        }

        @media screen and (max-width: 767px) {
            .width-modal {
                width: initial;
            }
        }

        table {
            display: block;
            overflow-x: auto;
            white-space: nowrap;
        }
        .page-content .dataTables_wrapper td, .page-content .dataTables_wrapper th {
            white-space: normal;
        }
        .table-responsive {
            overflow-x: auto !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 15px;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .dt-buttons .btn {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5;
            background: #36c6d3;
            border-color: #36c6d3;
            color: #fff!important;
            margin-right: 10px;
        }
        .dataTables_info {
            padding-top: 18px;
            padding-left: 15px;
        }
    </style>

    <script>
        $(".select2").select2();
        $(".select2").val("").trigger("change");

        $(".search-coroner").val(getParameterByName('dtv')).trigger("change");
        $(".search-surveyor").val(getParameterByName('ksv')).trigger("change");
        $(".search-criminal").val(getParameterByName('td')).trigger("change");

        $(".noidungvuan").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                window.location = urlSearch();
            }
        });
        $(".noidungtinbao").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                window.location = urlSearch();
            }
        });
        $(".ketquadieutra").bind('keyup',function(e) {
            if (e.keyCode === 13) {
                window.location = urlSearch();
            }
        });

        $(document.body).on("change",".search-coroner",function(){
            window.location = urlSearch();
        });

        $(document.body).on("change",".search-surveyor",function(){
            window.location = urlSearch();
        });

        $(document.body).on("change",".search-criminal",function(){
            window.location = urlSearch();
        });
        $(document.body).on("change",".search-tamdinhchibaotin",function(){
            window.location = urlSearch();
        });
        $(document.body).on("change",".search-tamdinhchivuan",function(){
            window.location = urlSearch();
        });
        $(document.body).on("change",".search-khoito",function(){
            window.location = urlSearch();
        });

        function urlSearch() {
            let noidungtinbao = 'noidungtinbao=' + $('.noidungtinbao').val();
            let noidungvuan = '&noidungvuan=' + $('.noidungvuan').val();
            let ketquadieutra = '&ketquadieutra=' + $('.ketquadieutra').val();
            let dtv = '&dtv=' + $('.search-coroner').val();
            let ksv = '&ksv=' + $('.search-surveyor').val();
            let td = '&td=' + $('.search-criminal').val();
            let tamdinhchibaotin = '&tamdinhchibaotin=' + $('.search-tamdinhchibaotin').val();
            let tamdinhchivuan = '&tamdinhchivuan=' + $('.search-tamdinhchivuan').val();
            let khoito = '&khoito=' + $('.search-khoito').val();

            return '/admin/news/list?'+noidungtinbao+noidungvuan+ketquadieutra+dtv+ksv+td+tamdinhchibaotin+tamdinhchivuan+khoito;
        }

        function getParameterByName(name, url = window.location.href) {
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

        $(document.body).on("change",".select-bienphamnganchan",function(){
            showHamTamGiam();
        });

        function showHamTamGiam() {
            let val = $('.select-bienphamnganchan').val();
            if(val==1 || val==2) {
                $('.form-hantamgiam').removeClass("hidden");
            } else {
                $('.form-hantamgiam').addClass("hidden");
            }
        }

        const urlGetNews = '/admin/news/getNews';
        const urlEdit = '/admin/news/edit';
        const urlList = '/admin/news/list';
        const urlDelete = '/admin/news/delete';
        const urlAdd = '/admin/news/add';

        //show dialog add
        $(".add-news").click(function(e) {
            e.preventDefault();
            //prosess form true is insert, false is update
            frmPerform(true);
        });

        //show dialog update
        $('#table tbody').on('click', '.update-news', function() {
            //prosess form true is insert, false is update
            self = this;
            frmPerform(false, self);
        });

        //load form true is insert, false is update
        function frmPerform(bln, self = null) {
            let txtTitle = "Thêm tin báo, vụ án";
            $("input[name='txtNoidngtinbao']").val("");
            $("input[name='txtNoidungvuan']").val("");
            $(".txtKetquadieutra").val("");
            $("input[name='txtSotinbao']").val("");
            $("input[name='txtNgaybatdautinbao']").val("");
            $("input[name='txtNgayketthuctinbao']").val("");
            $("input[name='txtSoKTVA']").val("");
            $("input[name='txtNgaybatdauktva']").val("");
            $("input[name='txtNgaykethucktva']").val("");
            $("input[name='handieutra']").val("");
            $("input[name='hantamgiam']").val("");

            $(".select-tamdinhchibaotin").val("1");
            $(".select-tamdinhchivuan").val("1");
            $(".select-khoito").val("1");
            $(".select-bienphamnganchan").val("0");

            $("input[name='txtId']").val("");

            //$(".select2").select2();
            //$(".select2").val("").trigger("change");

            $(".remove-bican .select2 ").remove();
            $(".select-bican").select2({ tags: true });
            $(".select-bican").val("").trigger("change");

            $(".select-bican option").each(function() {
                $(this).remove();
            });

            if(!bln) {
                txtTitle = "Sửa tin báo, vụ án";
                let data = {
                    "id": $(self).data("id")
                }
                let result = ajaxQuery(urlGetNews, data, 'POST');
                if(result.code==200) {
                    $("input[name='txtId']").val(result.data.id);
                    $("input[name='txtNoidngtinbao']").val(result.data.noidungtinbao);
                    $("input[name='txtNoidungvuan']").val(result.data.noidungvuan);
                    $("input[name='txtSotinbao']").val(result.data.sotinbao);
                    $("input[name='txtNgaybatdautinbao']").val(result.data.ngaybatdautinbao==null ? "" : formatDate(result.data.ngaybatdautinbao));
                    $("input[name='txtNgayketthuctinbao']").val(result.data.ngayketthuctinbao==null ? "" : formatDate(result.data.ngayketthuctinbao));
                    $("input[name='txtSoKTVA']").val(result.data.soktva);
                    $("input[name='txtNgaybatdauktva']").val(result.data.ngaybatdauktva==null ? "" : formatDate(result.data.ngaybatdauktva));
                    $("input[name='txtNgaykethucktva']").val(result.data.ngaykethucktva==null ? "" : formatDate(result.data.ngaykethucktva));
                    $("input[name='handieutra']").val(result.data.handieutra==null ? "" : formatDate(result.data.handieutra));
                    $("input[name='hantamgiam']").val(result.data.hantamgiam==null ? "" : formatDate(result.data.hantamgiam));
                    $(".txtKetquadieutra").val(result.data.ketquadieutra);

                    $(".select-tamdinhchibaotin").val(result.data.tamdinhchibaotin);
                    $(".select-tamdinhchivuan").val(result.data.tamdinhchivuan);
                    $(".select-khoito").val(result.data.khoito);
                    $(".select-bienphamnganchan").val(result.data.bienphapnganchan);

                    $(".select-coroner").val(result.data.dtv_id).trigger("change");
                    $(".select-surveyor").val(result.data.ksv_id).trigger("change");
                    $(".select-criminal").val(result.data.toidanh).trigger("change");

                    if(result.data.dsBican != null) {
                        Object.keys(result.data.dsBican).forEach(function(key) {
                            $(".select-bican").append(new Option(result.data.dsBican[key].name, result.data.dsBican[key].id));
                        });
                    }

                    $('.select-bican').val(result.data.bican).trigger('change');


                    $('.giahan').removeClass("hidden");
                    $("input[name='txtNgaybatdaugiahan']").val(result.data.ngaybatdaugiahantinbao==null ? "" : formatDate(result.data.ngaybatdaugiahantinbao));
                    $("input[name='txtNgayketthucgiahan']").val(result.data.ngaykethucgiahantinbao==null ? "" : formatDate(result.data.ngaykethucgiahantinbao));
                } else if(result.code==401) {
                    //noti
                    showAlert(result.message, result.notify, 5000);
                }

            }
            showHamTamGiam();
            //edit title form
            $('#modal-news .modal-title strong').html(txtTitle);
            //edit button form
            $("#btnAddNews").contents().last()[0].textContent = txtTitle;
        }

        //add news
        $("#btnAddNews").click(function(e) {
            e.preventDefault();
            let _token = $('meta[name="csrf-token"]').attr('content');
            let txtId = $("input[name='txtId']").val();
            let txtNoidngtinbao = $("input[name='txtNoidngtinbao']").val();
            let txtNoidungvuan = $("input[name='txtNoidungvuan']").val();
            let txtSotinbao = $("input[name='txtSotinbao']").val();
            let txtNgaybatdautinbao = $("input[name='txtNgaybatdautinbao']").val();
            let txtNgayketthuctinbao = $("input[name='txtNgayketthuctinbao']").val();
            let txtSoKTVA = $("input[name='txtSoKTVA']").val();
            let txtNgaybatdauktva = $("input[name='txtNgaybatdauktva']").val();
            let txtNgaykethucktva = $("input[name='txtNgaykethucktva']").val();
            let handieutra = $("input[name='handieutra']").val();
            let hantamgiam = $("input[name='hantamgiam']").val();
            let txtTamdinhchibaotin = $(".select-tamdinhchibaotin").val();
            let txtTamdinhchivuan = $(".select-tamdinhchivuan").val();
            let txtKhoito = $(".select-khoito").val();
            let txtBienphamnganchan = $(".select-bienphamnganchan").val();
            let txtCoroner = $(".select-coroner").val();
            let txtSurveyor = $(".select-surveyor").val();
            let txtCriminal = $(".select-criminal").val();

            let txtKetquadieutra = $(".txtKetquadieutra").val();
            let txtBican = [];
            if($(".select-bican").val() != null) {
                let li = $(".select2-selection__rendered").children();
                for(let i = 0; i < li.length; i++) {
                    if(li[i].className == 'select2-selection__choice') {
                        txtBican.push(li[i].attributes[1].nodeValue);
                    }
                }
            }

            let data = {
                "_token": _token,
                id: txtId,
                noidungtinbao: txtNoidngtinbao,
                noidungvuan: txtNoidungvuan,
                dtv_id: txtCoroner,
                ksv_id: txtSurveyor,
                toidanh: txtCriminal,
                ketquadieutra: txtKetquadieutra,
                sotinbao: txtSotinbao,
                ngaybatdautinbao: txtNgaybatdautinbao,
                ngayketthuctinbao: txtNgayketthuctinbao,
                tamdinhchibaotin: txtTamdinhchibaotin,
                soktva: txtSoKTVA,
                ngaybatdauktva: txtNgaybatdauktva,
                ngaykethucktva: txtNgaykethucktva,
                tamdinhchivuan: txtTamdinhchivuan,
                khoito: txtKhoito,
                bienphapnganchan: txtBienphamnganchan,
                bican: txtBican,
                handieutra: handieutra,
                hantamgiam: hantamgiam
            };
            if(txtId!='') {
                data.ngaybatdaugiahantinbao = $("input[name='txtNgaybatdaugiahan']").val();
                data.ngaykethucgiahantinbao = $("input[name='txtNgayketthucgiahan']").val();
            }


            //query add permission
            let result = ajaxQuery(txtId=='' ? urlAdd : urlEdit, data, 'POST');
            if(result.code == 200) {

                //close modal add
                $('#modal-news').modal('hide');
                $('.modal-backdrop').remove();

                //noti
                showAlert(result.message, result.notify, 5000);
                window.location.href = window.location.href;
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            } else {
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        });

        //prosess delete
        $(".btnDel").click(function(e) {
            var data = {
                "id": $(self).data("id"),
                "_token": $('meta[name="csrf-token"]').attr('content'),
            };
            //query del permission
            let result = ajaxQuery(urlDelete, data, 'POST');
            processResponse(result);
        });

        function processResponse(result) {
            if(result == undefined) {
                //noti
                showAlert("@Lang('global.msg_error')", "danger", 5000);
            }
            else if(result.code == 200) {
                //noti
                showAlert(result.message, result.notify, 5000);

                window.location.href = window.location.href;
            } else if(result.code == 401) {
                //noti
                showAlert(result.message, result.notify, 5000);
            }else{
                Object.keys(result.errors).forEach(function(key) {
                    //noti
                    showAlert(result.errors[key], result.notify, 5000);
                });
            }
        }

    </script>
@endsection
