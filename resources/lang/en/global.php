<?php

return [
    'notify_success' => 'success',
    'notify_danger' => 'danger',
    'msg_add_success' => 'Thêm thành công!',
    'msg_edit_success' => 'Sửa thành công!',
    'msg_delete_success' => 'Xóa thành công!',
    'msg_error' => 'Có lỗi, xin vui lòng thử lại!',
    'msg_validate' => 'Dữ liệu không hợp lệ, vui lòng nhập lại!'

];
