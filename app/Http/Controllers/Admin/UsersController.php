<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Models\User_history;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Exception;


class UsersController extends Controller

{
    public function __construct() {
        //$this->middleware(['isAdmin']);
    }

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function getList(Request $request)
    {
        $data = User::DataUsers(addslashes($request->txt_search));

        if($request->ajax()){
//            foreach ($data as $item) {
//                $item->roles = $item->roles()->value('name', 'name');
//            }

            return response_json(200, "", "", $data);
        }else{
            return view('admin.users.list', ['users' => $data]);
        }
    }

    public function getUsers(Request $request){
        $user = User::getUserById($request->id);
        return response_json(200, "", "", $user);
    }

    public function updateUser(UserRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", "danger", null, $request->validator->errors());
            }

            User::updateUser($request);

            return response_json(200, Lang::get('global.msg_edit_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function delUser(Request $request) {
        try {
            User::deleteUser($request->id);

            return response_json(200,  Lang::get('global.msg_delete_success'), Lang::get('global.notify_success'));

        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function addUser(UserRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            User::insertUser($request);

            return response_json(200, Lang::get('global.msg_add_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }

    }

    public function userHistory(Request $request) {
        $data = User_history::dataUsersHistory();

        if($request->ajax()){
            return response_json(200, "", "", $data);
        }else{
            return view('admin.users.user_history', ['data' => $data]);
        }
    }
}
