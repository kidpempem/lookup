<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Exception;


class ReportController extends Controller

{
    public function __construct() {
        //$this->middleware(['isAdmin']);
    }

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function getNews(Request $request) {
        //dd($request->noidungtinbao);
        $data = News::reportNews($request->noidungtinbao, $request->noidungvuan, $request->ketquadieutra, $request->dtv, $request->ksv, $request->td,
                                    $request->tamdinhchibaotin, $request->tamdinhchivuan, $request->khoito);
        if($request->ajax()){
            return response_json(200, "", "", $data);
        }else{
            return view('admin.report.news', ['news' => $data]);
        }
    }

}
