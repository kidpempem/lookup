<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CoronerRequest;
use App\Models\Coroner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Exception;


class CoronerController extends Controller

{
    public function __construct() {
        //$this->middleware(['isAdmin']);
    }

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function getList(Request $request) {
        $data = Coroner::getData($request->txt_search);
        if($request->ajax()){
            return response_json(200, "", "", $data);
        }else{
            return view('admin.coroner.list', ['coroner' => $data]);
        }
    }

    public function insertCoroner(CoronerRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            Coroner::insertCoroner($request);

            return response_json(200, Lang::get('global.msg_add_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function updateCoroner(CoronerRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            Coroner::updateCoroner($request);

            return response_json(200, Lang::get('global.msg_edit_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function delCoroner(Request $request) {
        try {
            Coroner::delCoroner($request->id);

            return response_json(200,  Lang::get('global.msg_delete_success'), Lang::get('global.notify_success'));

        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function getCoroner(Request $request){
        try {
            return response_json(200, "", "", Coroner::findCoroner($request->id));
        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }


}
