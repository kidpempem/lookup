<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accused;
use App\Models\Coroner;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class ExportController extends Controller

{
    public function collection()
    {
        $orders = News::all();
        foreach ($orders as $row) {
            $order[] = array(
                '0' => $row->id,
                '1' => number_format($row->total),
            );
        }

        return (collect($order));
    }

    public function headings()
    {
        return [
            'id',
            'Tổng',
        ];
    }

    public function export(Request $request){
        include(app_path().'/Helpers/PHPExcel.php');
        include(app_path().'/Helpers/PHPExcel/Writer/Excel2007.php');
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header('Content-Disposition: attachment; filename=Baocao_'.date("YmdHis").'.xls');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header("Cache-Control: private",false);
        ob_clean();
        ob_start();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->createSheet(); // tạo 1 sheet mới
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet->setTitle("Báo cáo"); // đặt tên sheet là tên tỉnh

        $activeSheet->setCellValue('A2', 'BẲNG THỐNG KÊ TIN BÁO, VỤ ÁN TỔ TỐ TỤNG ĐANG ĐIỀU TRA');
        $activeSheet->mergeCells("A2:S2");
        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FF0000'),
                'size'  => 20
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ));
        $activeSheet->getStyle('A2')->applyFromArray($styleArray);

        $style = array(
            'alignment' => array(
                'wrap' => true
            ),
            'borders' => array (
                'allborders' => array (
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'),        // BLACK
                )
            )
        );

        $styleCenter = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'BDD7EE')
            ),
            'font' => array(
                'bold' => true
            )
        );

        $activeSheet->freezePane('A5');
        $activeSheet->freezePane('A6');

        $activeSheet->getStyle('A4:S5')->applyFromArray($styleCenter);

        $activeSheet->mergeCells('A4:A5');
        $activeSheet->mergeCells('B4:B5');
        $activeSheet->mergeCells('C4:C5');
        $activeSheet->mergeCells('D4:D5');
        $activeSheet->mergeCells('E4:E5');
        $activeSheet->mergeCells('F4:F5');
        $activeSheet->mergeCells('G4:G5');
        $activeSheet->mergeCells('H4:H5');
        $activeSheet->mergeCells('I4:I5');
        $activeSheet->mergeCells('J4:J5');
        $activeSheet->mergeCells('K4:K5');
        $activeSheet->mergeCells('L4:L5');
        $activeSheet->mergeCells('M4:M5');
        $activeSheet->mergeCells('N4:N5');
        $activeSheet->mergeCells('O4:S4');

        $activeSheet->getColumnDimension('A')->setWidth(10);
        $activeSheet->getColumnDimension('B')->setWidth(30);
        $activeSheet->getColumnDimension('C')->setWidth(24);
        $activeSheet->getColumnDimension('D')->setWidth(12);
        $activeSheet->getColumnDimension('E')->setWidth(12);
        $activeSheet->getColumnDimension('F')->setWidth(15);
        $activeSheet->getColumnDimension('G')->setWidth(24);
        $activeSheet->getColumnDimension('H')->setWidth(24);
        $activeSheet->getColumnDimension('I')->setWidth(24);
        $activeSheet->getColumnDimension('J')->setWidth(24);
        $activeSheet->getColumnDimension('K')->setWidth(13);
        $activeSheet->getColumnDimension('L')->setWidth(13);
        $activeSheet->getColumnDimension('M')->setWidth(13);
        $activeSheet->getColumnDimension('N')->setWidth(20);

        //header
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'NỘI DUNG TIN BÁO')
            ->setCellValue('C4', 'NỘI DUNG VỤ ÁN')
            ->setCellValue('D4', 'ĐTV')
            ->setCellValue('E4', 'KSV')
            ->setCellValue('F4', 'TỘI DANH')
            ->setCellValue('G4', 'KẾT QUẢ DIỀU TRA')
            ->setCellValue('H4', 'SỐ, NGÀY PHÂN CÔNG TIN BÁO')
            ->setCellValue('I4', 'TẠM ĐÌNH CHỈ TIN BÁO')
            ->setCellValue('J4', 'SỐ, NGÀY KTVA')
            ->setCellValue('K4', 'TẠM ĐÌNH CHỈ VỤ ÁN')
            ->setCellValue('L4', 'KHÔNG KHỞI TỐ')
            ->setCellValue('M4', 'BỊ CAN')
            ->setCellValue('N4', 'NGÀY HẾT HẠN ĐIỀU TRA')
            ->setCellValue('O4', 'BIỆN PHÁP NGĂN CHẶN')
            ->setCellValue('O5', 'BẮT TẠM GIAM')
            ->setCellValue('P5', 'TẠM GIAM')
            ->setCellValue('Q5', 'CẤM')
            ->setCellValue('R5', 'BẢO LÃNH')
            ->setCellValue('S5', 'BPNC KHÁC');

        $data = News::reportNews($request->noidungtinbao, $request->noidungvuan, $request->ketquadieutra, $request->dtv, $request->ksv, $request->td,
            $request->tamdinhchibaotin, $request->tamdinhchivuan, $request->khoito);

        $i=6;
        $stt=1;
        foreach ($data as $item) {
            $bican = "";
            $lstBican = Accused::findAccused($item->id);

            if(!is_null($lstBican)) {
                foreach ($lstBican as $val) {
                    //array_push($bican, $val->name);
                    $bican .= $val->name . "\n";
                }
            }
            $ngBD = is_null($item->ngaybatdautinbao) ? "" : date('d/m/Y', strtotime($item->ngaybatdautinbao));
            $ngKT = is_null($item->ngayketthuctinbao) ? "" : date('d/m/Y', strtotime($item->ngayketthuctinbao));
            $giahan = "";
            if (!is_null($item->ngaybatdaugiahantinbao) || !is_null($item->ngaykethucgiahantinbao)) {
                $ngBDGH = is_null($item->ngaybatdaugiahantinbao) ? "" : date('d/m/Y', strtotime($item->ngaybatdaugiahantinbao));
                $ngKTGH = is_null($item->ngaykethucgiahantinbao) ? "" : date('d/m/Y', strtotime($item->ngaykethucgiahantinbao));
                $giahan = " Gia hạn " . $ngBDGH . ' - ' . $ngKTGH;
            }

            $ngBDVA = is_null($item->ngaybatdauktva) ? "" : date('d/m/Y', strtotime($item->ngaybatdauktva));
            $ngKTVA = is_null($item->ngaykethucktva) ? "" : date('d/m/Y', strtotime($item->ngaykethucktva));

            $activeSheet->setCellValue("A$i", $stt);
            $activeSheet->setCellValue("B$i", $item->noidungtinbao);
            $activeSheet->setCellValue("C$i", $item->noidungvuan);
            $activeSheet->setCellValue("D$i", !is_null($item->dtv_id) ? $item->parentCoronel->name : "");
            $activeSheet->setCellValue("E$i", !is_null($item->ksv_id) ? $item->parentSurveyor->name : "");
            $activeSheet->setCellValue("F$i", !is_null($item->ksv_id) ? $item->parentCriminal->name : "");
            $activeSheet->setCellValue("G$i", html_entity_decode($item->ketquadieutra), ENT_COMPAT, 'UTF-8');
            $activeSheet->setCellValue("H$i", 'Số '.$item->sotinbao.' ngày '.$ngBD.' - '.$ngKT."\n".$giahan);
            $activeSheet->setCellValue("I$i", $item->tamdinhchibaotin==0 ? 'Hoạt động bình thường' : 'Đình chỉ báo tin');
            $activeSheet->setCellValue("J$i", 'Số '.$item->soktva.' ngày '.$ngBDVA.' - '.$ngKTVA);
            $activeSheet->setCellValue("K$i", $item->tamdinhchivuan==0 ? 'Không đình chỉ' : 'Đình chỉ vụ án');
            $activeSheet->setCellValue("L$i", $item->khoito==0 ? 'Không khởi tố' : 'Khởi tố');
            $activeSheet->setCellValue("M$i", $bican);
            $activeSheet->setCellValue("N$i", !is_null($item->handieutra) ? date('d/m/Y', strtotime($item->hantamgiam)) : "");
            $activeSheet->setCellValue("O$i", $item->bienphapnganchan==1 ? !is_null($item->hantamgiam) ? date('d/m/Y', strtotime($item->hantamgiam)) : "" : "");
            $activeSheet->setCellValue("P$i", $item->bienphapnganchan==2 ? !is_null($item->hantamgiam) ? date('d/m/Y', strtotime($item->hantamgiam)) : "" : "");
            $activeSheet->setCellValue("Q$i", $item->bienphapnganchan==3 ? "x" : "");
            $activeSheet->setCellValue("R$i", $item->bienphapnganchan==4 ? "x" : "");
            $activeSheet->setCellValue("S$i", $item->bienphapnganchan==5 ? "x" : "");
            $i++;
            $stt++;
        }
        $endRanger = $i-1;
        $activeSheet->getStyle("A4:S$endRanger")->applyFromArray($style);


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('php://output');
        ob_flush();
        exit();
        //return Redirect::back();
    }
}
