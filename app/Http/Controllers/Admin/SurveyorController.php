<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SurveyorRequest;
use App\Models\Surveyor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Exception;


class SurveyorController extends Controller

{
    public function __construct() {
        //$this->middleware(['isAdmin']);
    }

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function getList(Request $request) {
        $data = Surveyor::getData($request->txt_search);
        if($request->ajax()){
            return response_json(200, "", "", $data);
        }else{
            return view('admin.surveyor.list', ['surveyor' => $data]);
        }
    }

    public function insertSurveyor(SurveyorRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            Surveyor::insertSurveyor($request);

            return response_json(200, Lang::get('global.msg_add_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function updateSurveyor(SurveyorRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            Surveyor::updateSurveyor($request);

            return response_json(200, Lang::get('global.msg_edit_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function delSurveyor(Request $request) {
        try {
            Surveyor::delSurveyor($request->id);

            return response_json(200,  Lang::get('global.msg_delete_success'), Lang::get('global.notify_success'));

        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function getSurveyor(Request $request){
        try {
            return response_json(200, "", "", Surveyor::findSurveyor($request->id));
        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }


}
