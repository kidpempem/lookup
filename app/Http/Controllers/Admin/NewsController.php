<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use mysql_xdevapi\Exception;


class NewsController extends Controller

{
    public function __construct() {
        //$this->middleware(['isAdmin']);
    }

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function getList(Request $request) {
        $data = News::reportNews($request->noidungtinbao, $request->noidungvuan, $request->ketquadieutra, $request->dtv, $request->ksv, $request->td,
                                    $request->tamdinhchibaotin, $request->tamdinhchivuan, $request->khoito);

        if($request->ajax()){
            return response_json(200, "", "", $data);
        }else{
            return view('admin.news.list', ['news' => $data]);
        }
    }

    public function insertNews(NewsRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            News::insertNews($request);

            return response_json(200, Lang::get('global.msg_add_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function updateNews(NewsRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", Lang::get('global.notify_danger'), null, $request->validator->errors());
            }

            News::updateNews($request);

            return response_json(200, Lang::get('global.msg_edit_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function delNews(Request $request) {
        try {
            News::delNews($request->id);

            return response_json(200,  Lang::get('global.msg_delete_success'), Lang::get('global.notify_success'));

        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function getNews(Request $request){
        try {
            return response_json(200, "", "", News::findNews($request->id));
        } catch (Exception $ex) {
            return response_json(0, Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }


}
