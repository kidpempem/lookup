<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu_admin;
use Illuminate\Http\Request;
use App\Models\User;



class DashboardController extends Controller

{
    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
        return view('admin.dashboard.index');
    }

}
