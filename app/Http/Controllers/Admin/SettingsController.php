<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Models\General;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;

class SettingsController extends Controller

{
    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function menu()

    {
        return view('admin.settings.menu');
    }

    public function general() {
        return view('admin.settings.general', ['data' => General::GetGeneral()]);
    }

    public function editGeneral(Request $request) {
        try {
            General::editGeneral($request);

            return response_json(200, Lang::get('global.msg_edit_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

    public function profile() {
        return view('admin.settings.profile', ['data' => auth()->user()]);
    }

    public function editProfile(UserRequest $request) {
        try {
            //validator
            if (isset($request->validator) && $request->validator->fails()) {
                return response_json(0, "", "danger", null, $request->validator->errors());
            }
            General::editProfile($request);

            return response_json(200, Lang::get('global.msg_edit_success'), Lang::get('global.notify_success'));
        } catch (Exception $ex) {
            return response_json(0,  Lang::get('global.msg_error'), Lang::get('global.notify_danger'));
        }
    }

}
