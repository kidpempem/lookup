<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'FirstName'                 => 'required',
            'LastName'                  => 'required',
            'Email'                     => 'required|email|unique:users,Email,'.$this->id,
        ];

        if(empty($this->id)) {
            $rules['Password'] = 'required|same:Password_Confirm';
            $rules['Password_Confirm'] = 'required';
        }
        else if(!empty($this->id) && !is_null($this->Password))
        {
            $rules['Password'] = 'same:Password_Confirm';
        }

        return $rules;
    }

    public function messages(){
        return [
            'FirstName.required'        => 'First Name không được để trống!',

            'LastName.required'         => 'Last Name không được để trống!',

            'Email.required'            => 'Email không được để trống!',
            'Email.email'               => 'Email không hợp lệ!',
            'Email.unique'              => 'Email đã tồn tại!',

            'Password.required'         => 'Mật khẩu không được để trống!',
            'Password_Confirm.required' => 'Nhập lại mật khẩu không được để trống!',
            'Password.same'             => 'Mật khẩu không khớp!'
        ];
    }

    public $validator = null;
    protected function failedValidation($validator)
    {
        $this->validator = $validator;
    }

}
