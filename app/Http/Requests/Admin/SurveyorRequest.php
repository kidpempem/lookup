<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SurveyorRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'                      => 'required',
            'phone'                     => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10|unique:khaosatvien,phone,'.$this->id,
            'email'                     => 'required|email|unique:khaosatvien,email,'.$this->id,
        ];

        return $rules;
    }

    public function messages(){
        return [
            'name.required'             => 'Tên không được để trống!',

            'email.required'            => 'Email không được để trống!',
            'email.email'               => 'Email không hợp lệ!',
            'email.unique'              => 'Email đã tồn tại!',

            'phone.required'            => 'Số điện thoại không được để trống!',
            'phone.regex'               => 'Số điện thoại không đúng định dạng!',
            'phone.min'                 => 'Số điện thoại không đúng định dạng!',
            'phone.unique'              => 'Số điện thoại đã tồn tại!',
        ];
    }

    public $validator = null;
    protected function failedValidation($validator)
    {
        $this->validator = $validator;
    }

}
