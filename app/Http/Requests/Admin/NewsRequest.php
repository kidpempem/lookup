<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'noidungtinbao'                      => 'required',
            'noidungvuan'                        => 'required'
        ];

        return $rules;
    }

    public function messages(){
        return [
            'noidungtinbao.required'             => 'Nội dung tin báo không được để trống!',
            'noidungvuan.required'            => 'Nội dung vụ án không được để trống!',
        ];
    }

    public $validator = null;
    protected function failedValidation($validator)
    {
        $this->validator = $validator;
    }

}
