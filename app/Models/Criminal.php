<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Criminal extends Model {

    protected $table = 'criminal';

    public static function getData($key) {
        return Criminal::whereLike(['name'], $key)->orderBy('id', 'DESC')->paginate(10);

    }

    public static function insertCriminal($current) {
        try {
            $criminal = new Criminal();

            $criminal->name = $current->name;
            $criminal->status = $current->status;
            $criminal->save();
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function updateCriminal($current) {
        try {
            $criminal = Criminal::findOrFail($current->id);

            $criminal->name = $current->name;
            $criminal->status = $current->status;
            $criminal->save();

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function delCriminal($id) {
        try {
            $criminal = Criminal::findOrFail($id);
            $criminal->delete($criminal->id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function findCriminal($id) {
        try {
            return Criminal::findOrFail($id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getAllCriminal() {
        return Criminal::where('status', 1)->orderBy('id', 'DESC')->get();
    }
}
