<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu_admin extends Model {

    protected $table = 'menu_admin';

    public static function Menu_Settings($parent_id) {

        return Menu_admin::where('parent_id', $parent_id)->orderBy('order')->get();
    }

}
