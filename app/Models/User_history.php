<?php namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User_history extends Model {

    protected $table = 'user_history';

    public static function insertHistory($action, $note) {
        $userHistory = new User_history();
        $userHistory->user_id = Auth::user()->id;
        $userHistory->action = $action;
        $userHistory->note = $note;
        $userHistory->save();
    }

    public function parent() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function dataUsersHistory() {
        $data = User_history::orderby('id', 'DESC')->paginate(20);
        foreach ($data as $item) {
            $item->name = $item->parent->name;
        }

        return $data;
    }

}
