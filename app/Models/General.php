<?php namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Cache;

class General extends Model {

    protected $table = 'general';

    public static function GetGeneral() {
        return General::all()->first();
    }
    public static function editGeneral($current) {
        try {
            $general = General::findOrFail($current->id);
            $general->name = $current->name;
            $general->email = $current->email;
            $general->address = $current->address;
            $general->phone = $current->phone;
            $general->meta_title = $current->metaTitle;
            $general->meta_description = $current->metaDesc;
            $general->meta_keyword = $current->metaKeyword;
            $general->logo = $current->logo;
            $general->favicon = $current->favicon;
            $general->bannerOne = $current->bannerOne;
            $general->bannerTwo = $current->bannerTwo;
            $general->analytics = $current->analytics;
            $general->description = $current->desc;
            $general->slogan = $current->slogan;
            $general->gg_map = $current->map;
            $general->link_video = $current->linkvideo;
            $general->contacts = $current->contacts;
            $general->menu_top = $current->menu_top;
            $general->menu_footer = $current->menu_footer;
            $general->menu_left = $current->menu_left;
            $general->save();

            Cache::forever('settings', $general);

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function editProfile($profile) {
        try {
            $user = User::find($profile->id);
            $user->FirstName = $profile->FirstName;
            $user->LastName = $profile->LastName;
            $user->name = $profile->FirstName . ' ' . $profile->LastName;
            $user->email = $profile->Email;
            if(!is_null($profile->Password)) {
                $user->password = bcrypt($profile->Password);
            }

            $user->save();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

}
