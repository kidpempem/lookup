<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accused extends Model {

    protected $table = 'bican';

    public static function getData($key) {
        return Coroner::whereLike(['name'], $key)->orderBy('id', 'DESC')->paginate(10);
    }

    public static function insertAccused($name, $id) {
        $accused = new Accused();
        $accused->name = $name;
        $accused->thongtinvuan_id = $id;
        $accused->save();
    }

    public static function findAccused($id) {
        return Accused::where('thongtinvuan_id', $id)->get();
    }

    public static function deleteAccused($foreignKey) {
        Accused::where('thongtinvuan_id', $foreignKey)->get()->each(function ($accused, $key) {
            $accused->delete();
        });
    }

    public static function getAllAccused() {
        return Coroner::orderBy('id', 'DESC')->get();
    }
}
