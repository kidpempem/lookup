<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model {

    protected $table = 'thongtinvuan';

    public function parentCoronel() {
        return $this->hasOne(Coroner::class, 'id', 'dtv_id');
    }

    public function parentSurveyor() {
        return $this->hasOne(Surveyor::class, 'id', 'ksv_id');
    }

    public function parentCriminal() {
        return $this->hasOne(Criminal::class, 'id', 'toidanh');
    }


    public static function getData($key) {
        $data = News::whereLike(['noidungtinbao'], $key)->orderBy('id', 'DESC')->paginate(10);
        foreach ($data as $item) {
            $item->dtv = $item->parentCoronel!=null ? $item->parentCoronel->name : "";
            $item->ksv = $item->parentSurveyor!=null ? $item->parentSurveyor->name : "";
            $item->td = $item->parentCriminal!=null ? $item->parentCriminal->name : "";
        }
        return $data;
    }

    public static function insertNews($current) {
        try {
            $news = new News();

            $news->noidungtinbao = $current->noidungtinbao;
            $news->noidungvuan = $current->noidungvuan;
            $news->dtv_id = $current->dtv_id;
            $news->ksv_id = $current->ksv_id;
            $news->toidanh = $current->toidanh;
            $news->ketquadieutra = $current->ketquadieutra;
            $news->sotinbao = $current->sotinbao;
            $news->ngaybatdautinbao = $current->ngaybatdautinbao;
            $news->ngayketthuctinbao = $current->ngayketthuctinbao;
            $news->tamdinhchibaotin = $current->tamdinhchibaotin;
            $news->soktva = $current->soktva;
            $news->ngaybatdauktva = $current->ngaybatdauktva;
            $news->ngaykethucktva = $current->ngaykethucktva;
            $news->tamdinhchivuan = $current->tamdinhchivuan;
            $news->khoito = $current->khoito;
            $news->bienphapnganchan = $current->bienphapnganchan;
            $news->handieutra = $current->handieutra;
            $news->hantamgiam = $current->hantamgiam;
            $news->save();

            if(!is_null($current->bican)) {
                foreach ($current->bican as $item) {
                    Accused::insertAccused($item, $news->id);
                }
            }

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function updateNews($current) {
        try {
            $news = News::findOrFail($current->id);

            $news->noidungtinbao = $current->noidungtinbao;
            $news->noidungvuan = $current->noidungvuan;
            $news->dtv_id = $current->dtv_id;
            $news->ksv_id = $current->ksv_id;
            $news->toidanh = $current->toidanh;
            $news->ketquadieutra = $current->ketquadieutra;
            $news->sotinbao = $current->sotinbao;
            $news->ngaybatdautinbao = $current->ngaybatdautinbao;
            $news->ngayketthuctinbao = $current->ngayketthuctinbao;
            $news->tamdinhchibaotin = $current->tamdinhchibaotin;
            $news->soktva = $current->soktva;
            $news->ngaybatdauktva = $current->ngaybatdauktva;
            $news->ngaykethucktva = $current->ngaykethucktva;
            $news->tamdinhchivuan = $current->tamdinhchivuan;
            $news->khoito = $current->khoito;
            $news->bienphapnganchan = $current->bienphapnganchan;
            $news->ngaybatdaugiahantinbao = $current->ngaybatdaugiahantinbao;
            $news->ngaykethucgiahantinbao = $current->ngaykethucgiahantinbao;
            $news->handieutra = $current->handieutra;
            $news->hantamgiam = $current->hantamgiam;

            $news->save();

            if(!is_null($current->bican)) {
                Accused::deleteAccused($news->id);
                foreach ($current->bican as $item) {
                    Accused::insertAccused($item, $news->id);
                }
            }

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function delNews($id) {
        try {
            $news = News::findOrFail($id);
            $news->delete($news->id);

            Accused::deleteAccused($news->id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function findNews($id) {
        try {
            $data = News::findOrFail($id);
            $childs = [];
            $dsChilds = [];
            if(!is_null($data->childrens)) {
                foreach ($data->childrens as $c) {
                    array_push($childs, $c->id);
                    array_push($dsChilds, $c);
                }
            }
            $data->bican = $childs;
            $data->dsBican = $dsChilds;
            return $data;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function childrens() {
        return $this->hasMany(Accused::class, 'thongtinvuan_id', 'id');
    }

    public static function reportNews($noidungtinbao, $noidungvuan, $ketquadieutra, $dtv, $ksv, $td, $tamdinhchibaotin, $tamdinhchivuan, $khoito, $flag=false) {
//        \DB::enableQueryLog();
//        $data = \DB::table('thongtinvuan')->select('noidungtinbao', 'noidungvuan', 'dtv_id', 'coroner.name as dtv', 'ksv_id', 'khaosatvien.name as ksv', 'toidanh', 'criminal.name as td',
//                                                    'ketquadieutra', 'tamdinhchibaotin as tamdinhchibaotin_id',
//                                                    \DB::raw("(CASE WHEN tamdinhchibaotin = 0 THEN 'Hoạt động bình thường' ELSE 'Đình chỉ báo tin' END) AS tamdinhchibaotin"),
//                                                    'tamdinhchivuan as tamdinhchivuan_id',
//                                                    \DB::raw("(CASE WHEN tamdinhchivuan = 0 THEN 'Không đình chỉ' ELSE 'Đình chỉ vụ án' END) AS tamdinhchivuan"),
//                                                    'khoito as khoito_id',
//                                                    \DB::raw("(CASE WHEN khoito = 0 THEN 'Không khởi tố' ELSE 'Khởi tố' END) AS khoito"))
//                ->leftjoin('coroner', 'coroner.id', "=", "thongtinvuan.dtv_id")
//                ->leftjoin('khaosatvien', 'khaosatvien.id', "=", "thongtinvuan.ksv_id")
//                ->leftjoin('criminal', 'criminal.id', "=", "thongtinvuan.toidanh");
//
//        foreach ($data as $item) {
//            $bican = [];
//            $lstBican = Accused::findAccused($item->id);
//            if (!is_null($lstBican)) {
//                foreach ($lstBican as $val) {
//                    array_push($bican, $val->name);
//                }
//            }
//            $item->bican = implode("<br>", $bican);
//        }
//        if(!is_null($noidungtinbao)) $data->where('noidungtinbao', 'like', '%'.$noidungtinbao.'%');
        //dd(\DB::getQueryLog());
        $data = News::orderBy('id', 'DESC');
        if(!is_null($noidungtinbao)) $data->where('noidungtinbao', 'like', '%'.$noidungtinbao.'%');
        if(!is_null($noidungvuan)) $data->where('noidungvuan', 'like', '%'.$noidungvuan.'%');
        if(!is_null($ketquadieutra)) $data->where('ketquadieutra', 'like', '%'.$ketquadieutra.'%');
        if($dtv!=0) $data->where('dtv_id', $dtv);
        if($ksv!=0) $data->where('ksv_id', $ksv);
        if($td!=0) $data->where('toidanh', $td);
        if(!is_null($tamdinhchibaotin)) $data->where('tamdinhchibaotin', $tamdinhchibaotin);
        if(!is_null($tamdinhchivuan)) $data->where('tamdinhchivuan', $tamdinhchivuan);
        if(!is_null($khoito)) $data->where('khoito', $khoito);
        foreach ($data as $item) {
            $item->dtv = $item->parentCoronel!=null ? $item->parentCoronel->name : "";
            $item->ksv = $item->parentSurveyor!=null ? $item->parentSurveyor->name : "";
            $item->td = $item->parentCriminal!=null ? $item->parentCriminal->name : "";
            //bi can
            $bican = [];
            $lstBican = Accused::findAccused($item->id);

            if(!is_null($lstBican)) {
                foreach ($lstBican as $val) {
                    array_push($bican, $val->name);
                }
            }
            $item->bican = implode("<br>", $bican);
        }
        if($flag) return $data->get();

        return $data->paginate(10);
    }
}
