<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Surveyor extends Model {

    protected $table = 'khaosatvien';

    public static function getData($key) {
        return Surveyor::whereLike(['name'], $key)->orderBy('id', 'DESC')->paginate(10);

    }

    public static function insertSurveyor($current) {
        try {
            $surveyor = new Surveyor();
            $surveyor->name = $current->name;
            $surveyor->email = $current->email;
            $surveyor->phone = $current->phone;
            $surveyor->titles = $current->titles;
            $surveyor->status = $current->status;
            $surveyor->save();
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function updateSurveyor($current) {
        try {
            $surveyor = Surveyor::findOrFail($current->id);

            $surveyor->name = $current->name;
            $surveyor->email = $current->email;
            $surveyor->phone = $current->phone;
            $surveyor->titles = $current->titles;
            $surveyor->status = $current->status;
            $surveyor->save();

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function delSurveyor($id) {
        try {
            $surveyor = Surveyor::findOrFail($id);
            $surveyor->delete($surveyor->id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function findSurveyor($id) {
        try {
            return Surveyor::findOrFail($id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getAllSurveyor() {
        return Surveyor::where('status', 1)->orderBy('id', 'DESC')->get();
    }
}
