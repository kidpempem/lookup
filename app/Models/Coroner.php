<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coroner extends Model {

    protected $table = 'coroner';

    public static function getData($key) {
        return Coroner::whereLike(['name'], $key)->orderBy('id', 'DESC')->paginate(10);

    }

    public static function insertCoroner($current) {
        try {
            $coroner = new Coroner();

            $coroner->name = $current->name;
            $coroner->phone = $current->phone;
            $coroner->email = $current->email;
            $coroner->address = $current->address;
            $coroner->status = $current->status;
            $coroner->save();
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function updateCoroner($current) {
        try {
            $coroner = Coroner::findOrFail($current->id);

            $coroner->name = $current->name;
            $coroner->phone = $current->phone;
            $coroner->email = $current->email;
            $coroner->address = $current->address;
            $coroner->status = $current->status;
            $coroner->save();

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function delCoroner($id) {
        try {
            $coroner = Coroner::findOrFail($id);
            $coroner->delete($coroner->id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function findCoroner($id) {
        try {
            return Coroner::findOrFail($id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getAllCoroner() {
        return Coroner::where('status', 1)->orderBy('id', 'DESC')->get();
    }
}
