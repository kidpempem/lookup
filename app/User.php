<?php

namespace App;

use App\Models\User_history;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function DataUsers($key) {
        $data = User::whereLike(['name', 'email', 'FirstName'], $key)->orderBy('id', 'DESC')->paginate(10);
        //history
        User_history::insertHistory('View user', 'Xem danh sách người dùng');
        return $data;
    }

    public static function getUserById($id) {
        $user = User::findOrFail($id);
        //$user->roles = $user->roles()->value('id', 'id');

        return $user;
    }

    public static function updateUser($currentUser) {
        try {
            $user = User::find($currentUser->id);
            $user->FirstName = $currentUser->FirstName;
            $user->LastName = $currentUser->LastName;
            $user->name = $currentUser->FirstName . ' ' . $currentUser->LastName;
            $user->email = $currentUser->Email;
            if(!is_null($currentUser->Password)) {
                $user->password = bcrypt($currentUser->Password);
            }
            $user->status = $currentUser->Status;

            $user->save();

            //assignRole to user
//            $roles = $currentUser->Roles ? $currentUser->Roles : [];
//            $user->syncRoles($roles);
            //history
            User_history::insertHistory('Edit user', 'Sửa người dùng');

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public static function insertUser($currentUser) {
        try {
            $user = new User();
            $user->FirstName = $currentUser->FirstName;
            $user->LastName = $currentUser->LastName;
            $user->name = $currentUser->FirstName . ' ' . $currentUser->LastName;
            $user->email = $currentUser->Email;
            $user->password = bcrypt($currentUser->Password);
            $user->status = $currentUser->Status;
            $user->save();

//            $roles = $currentUser->input('Roles') ? $currentUser->input('Roles') : [];
//
//            $user->assignRole($roles);

            //history
            User_history::insertHistory('Add user', 'Thêm người dùng');

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public static function deleteUser($id) {
        try {
            $user = User::find($id);
            $user->delete($user->id);

            //history
            User_history::insertHistory('Xóa user', 'Xóa người dùng');
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}
