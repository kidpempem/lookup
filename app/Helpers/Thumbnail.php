<?php

namespace App\Helpers;

use Intervention\Image\Facades\Image;

class Thumbnail
{
    public static function resizeImage($sourceFile, $width, $height) {
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $sourceFile)) return null;
        $lastIndex = strripos($sourceFile, '/');
        $arrSource = explode("/", $sourceFile);

        //example image.png
        $img_name = end($arrSource);
        //example name
        $file_name = explode(".", $img_name);
        //folder image
        $folder = substr($sourceFile, 0, $lastIndex);
        //folder image root
        $folder_absolute = $_SERVER['DOCUMENT_ROOT'] . '/' . $folder . '/' . $width . 'x' . $height;
        $file = $folder_absolute . '/' . $file_name[0] . '.png';
        if (!file_exists($file)) {
            if(!file_exists($folder_absolute)) {
                mkdir($folder_absolute, 0777, true);
            }
            Image::make($_SERVER['DOCUMENT_ROOT'] . '/' .$sourceFile)->encode('png', 90)->resize($width, $height)->save($folder_absolute . '/' . $file_name[0] . '.png');
        }

        $folder_resize = $folder . '/' . $width . 'x' . $height . '/' . $file_name[0] .'.png';

        return $folder_resize;
    }
}
