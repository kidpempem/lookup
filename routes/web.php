<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('login', [App\Http\Controllers\Auth\LoginController::class, 'getLogin'])->name('login');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'postLogin']);
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');

    //manager users
    Route::group(['prefix'=>'system/users'], function() {
        Route::get('list', [App\Http\Controllers\Admin\UsersController::class, 'getList']);
        Route::post('getUsers', [App\Http\Controllers\Admin\UsersController::class, 'getUsers']);
        Route::post('delete', [App\Http\Controllers\Admin\UsersController::class, 'delUser']);
        Route::post('edit', [App\Http\Controllers\Admin\UsersController::class, 'updateUser']);
        Route::post('add', [App\Http\Controllers\Admin\UsersController::class, 'addUser']);
        Route::get('user_history', [App\Http\Controllers\Admin\UsersController::class, 'userHistory']);
    });

    Route::group(['prefix'=>'coroner'], function() {
        Route::get('list', [App\Http\Controllers\Admin\CoronerController::class, 'getList']);
        Route::post('add', [App\Http\Controllers\Admin\CoronerController::class, 'insertCoroner']);
        Route::post('edit', [App\Http\Controllers\Admin\CoronerController::class, 'updateCoroner']);
        Route::post('delete', [App\Http\Controllers\Admin\CoronerController::class, 'delCoroner']);
        Route::post('getCoroner', [App\Http\Controllers\Admin\CoronerController::class, 'getCoroner']);
    });

    Route::group(['prefix'=>'criminal'], function() {
        Route::get('list', [App\Http\Controllers\Admin\CriminalController::class, 'getList']);
        Route::post('add', [App\Http\Controllers\Admin\CriminalController::class, 'insertCriminal']);
        Route::post('edit', [App\Http\Controllers\Admin\CriminalController::class, 'updateCriminal']);
        Route::post('delete', [App\Http\Controllers\Admin\CriminalController::class, 'delCriminal']);
        Route::post('getCriminal', [App\Http\Controllers\Admin\CriminalController::class, 'getCriminal']);
    });

    Route::group(['prefix'=>'surveyor'], function() {
        Route::get('list', [App\Http\Controllers\Admin\SurveyorController::class, 'getList']);
        Route::post('add', [App\Http\Controllers\Admin\SurveyorController::class, 'insertSurveyor']);
        Route::post('edit', [App\Http\Controllers\Admin\SurveyorController::class, 'updateSurveyor']);
        Route::post('delete', [App\Http\Controllers\Admin\SurveyorController::class, 'delSurveyor']);
        Route::post('getSurveyor', [App\Http\Controllers\Admin\SurveyorController::class, 'getSurveyor']);
    });

    Route::group(['prefix'=>'news'], function() {
        Route::get('list', [App\Http\Controllers\Admin\NewsController::class, 'getList']);
        Route::post('add', [App\Http\Controllers\Admin\NewsController::class, 'insertNews']);
        Route::post('edit', [App\Http\Controllers\Admin\NewsController::class, 'updateNews']);
        Route::post('delete', [App\Http\Controllers\Admin\NewsController::class, 'delNews']);
        Route::post('getNews', [App\Http\Controllers\Admin\NewsController::class, 'getNews']);
        Route::get('export', [App\Http\Controllers\Admin\ExportController::class, 'export']);
    });

    Route::group(['prefix'=>'report'], function() {
        Route::get('news', [App\Http\Controllers\Admin\ReportController::class, 'getNews']);
    });
});
