function ckeditor(name) {
    var editor = CKEDITOR.replace(name,{
        uiColor: '#518ca3',
        language: 'vi',
    });
}

function comConfirm(msg){
    if (window.confirm(msg)) {
        return true;
    }
    return false;
}


//choose avatar image
function BrowseServer() {
    CKFinder.popup({
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var file = evt.data.files.first();
                var value_image = document.getElementById('Image');
                var value_file = document.getElementById('xFile');
                value_image.value = file.getUrl();
                value_file.src = file.getUrl();
            });
        }
    });
}
//remove image
var samplePhoto = '/uploads/placeholder.png';
$(".btn_remove_image").click(function () {
    $("#xFile").attr("src", samplePhoto);
});

//remove image
$('#list-photos-items').on('click', '.btn_remove_image', function(e) {
    e.preventDefault();
    $(this).parent().parent().remove();

    //array value image
    let images = [];
    $(".gallery_image_wrapper img").each(function () {
        images.push($(this).attr('src'))
    });
    $('#gallery-data').val(images);
    if(images.length==0) $(".reset-gallery").addClass("hidden");
});

//choose Gallery images
function galleryImages() {
    CKFinder.popup({
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var file = evt.data.files.first();
                if (file !== null){
                    $("#list-photos-items").append("<div class='col-md-2 col-sm-3 col-4 photo-gallery-item'>" +
                                                   "<div class='gallery_image_wrapper'>" +
                                                   "<img src='" + file.getUrl() + "'>" +
                                                    "<a class='btn_remove_image' title='Remove image'><i class='fa fa-times'></i></a>" +
                                                   "</div></div>");
                    //show reset class
                    $(".reset-gallery").removeClass("hidden");

                    //array value image
                    var images = [];
                    $(".gallery_image_wrapper img").each(function () {
                        images.push($(this).attr('src'))
                    })
                    console.log(images);
                    $('#gallery-data').val(images);
                }
            });
        }
    });
}

//remove all images
function resetImages() {
    $("#list-photos-items div").remove();

    //hidden reset class
    $(".reset-gallery").addClass("hidden");

}

//show notification success
function showNotiSuccess(message) {
    $('.toast-title').html("Success");
    $('.toast-message').html(message);
    $("#toast-container .toast").addClass("toast-success");

    $('#delUser').modal('toggle');
    $('#toast-container').show().delay(5000).slideUp();
}

//show notification error
function showNotiError(message) {
    $('.toast-title').html("Error");
    $('.toast-message').html(message);
    $("#toast-container .toast").addClass("toast-error");

    $('#delUser').modal('toggle');
    $('#toast-container').show().delay(5000).slideUp();
}

//load page
function locationPage(url) {
    if (!!!url) {
        window.location.reload();
    } else {
        window.location.href = url;
    }
}

function formatDatetime(datetime) {
   return moment(datetime).tz( "Asia/Bangkok").format("YYYY-MM-DD hh:mm:ss");
}

function formatDate(date) {
    return moment(date).format("YYYY-MM-DD");
}

function showAlert(message, type, closeDelay) {

    var $cont = $("#alerts-container");

    if ($cont.length == 0) {
        // alerts-container does not exist, create it
        $cont = $('<div id="alerts-container">')
            .css({
                position: "fixed"
                ,width: "16%"
                ,right: "40px"
                ,bottom: "0"
            })
            .appendTo($("body"));
    }

    // default to alert-info; other options include success, warning, danger
    type = type || "info";

    // create the alert div
    var alert = $('<div>')
        .addClass("fade in show alert alert-" + type)
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
                .append("&times;")
        )
        .append('<i class="mdi mdi-alert" aria-hidden="true"></i>')
        .append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $cont.prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);
}

function toSlug(str) {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str
        .normalize('NFD') // chuyển chuỗi sang unicode tổ hợp
        .replace(/[\u0300-\u036f]/g, ''); // xóa các ký tự dấu sau khi tách tổ hợp

    // Thay ký tự đĐ
    str = str.replace(/[đĐ]/g, 'd');

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, '-');

    // Xóa ký tự - liên tiếp
    str = str.replace(/-+/g, '-');

    // xóa phần dư - ở đầu & cuối
    str = str.replace(/^-+|-+$/g, '');

    // return
    return str;
}
