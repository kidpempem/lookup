/*
 Navicat Premium Data Transfer

 Source Server         : localhost73
 Source Server Type    : MySQL
 Source Server Version : 100418
 Source Host           : localhost:3306
 Source Schema         : lookup

 Target Server Type    : MySQL
 Target Server Version : 100418
 File Encoding         : 65001

 Date: 13/09/2021 14:45:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bican
-- ----------------------------
DROP TABLE IF EXISTS `bican`;
CREATE TABLE `bican`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `thongtinvuan_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bican
-- ----------------------------
INSERT INTO `bican` VALUES (41, 'tiến 1', '2021-09-12 20:34:07', '2021-09-12 20:34:07', 14);
INSERT INTO `bican` VALUES (42, 'hoàng', '2021-09-12 20:34:17', '2021-09-12 20:34:17', 4);
INSERT INTO `bican` VALUES (43, 'vàng', '2021-09-12 20:34:17', '2021-09-12 20:34:17', 4);

-- ----------------------------
-- Table structure for coroner
-- ----------------------------
DROP TABLE IF EXISTS `coroner`;
CREATE TABLE `coroner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coroner
-- ----------------------------
INSERT INTO `coroner` VALUES (6, 'Hoàng Mạnh', '0123456789', 'homico.tbd@gmail.com', 'Địa chỉ: Số 53, Liền kề 01, KĐT An Hưng, Dương Nội, Hà Đông, Hà Nội', 1, '2021-09-11 04:59:22', '2021-09-11 18:14:29');
INSERT INTO `coroner` VALUES (7, 'Lương Hải', '0932362631', 'manhtien150291@gmail.com', 'Địa chỉ: Số 53, Liền kề 01, KĐT An Hưng, Dương Nội, Hà Đông, Hà Nội', 1, '2021-09-11 18:14:43', '2021-09-11 18:14:43');
INSERT INTO `coroner` VALUES (8, 'test', '0966547199', 'homi1co.tbd@gmail.com', 'Địa chỉ: Số 53, Liền kề 01, KĐT An Hưng, Dương Nội, Hà Đông, Hà Nội', 2, '2021-09-11 18:14:58', '2021-09-11 18:15:04');

-- ----------------------------
-- Table structure for criminal
-- ----------------------------
DROP TABLE IF EXISTS `criminal`;
CREATE TABLE `criminal`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of criminal
-- ----------------------------
INSERT INTO `criminal` VALUES (2, 'Đèn Led Đường Phố', 2, '2021-09-11 05:18:56', '2021-09-11 05:18:56');
INSERT INTO `criminal` VALUES (3, 'Quản lý quyền', 1, '2021-09-11 05:19:30', '2021-09-11 05:19:30');
INSERT INTO `criminal` VALUES (4, 'Toto', 1, '2021-09-11 12:22:57', '2021-09-11 12:22:57');
INSERT INTO `criminal` VALUES (5, 'Quản lý người dùng', 1, '2021-09-11 12:23:26', '2021-09-11 12:23:26');
INSERT INTO `criminal` VALUES (6, '1', 1, '2021-09-11 12:26:04', '2021-09-11 12:26:04');
INSERT INTO `criminal` VALUES (7, 'Đèn Led Đường Phố', 1, '2021-09-11 12:26:51', '2021-09-11 12:26:51');

-- ----------------------------
-- Table structure for khaosatvien
-- ----------------------------
DROP TABLE IF EXISTS `khaosatvien`;
CREATE TABLE `khaosatvien`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `titles` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of khaosatvien
-- ----------------------------
INSERT INTO `khaosatvien` VALUES (1, 'Đèn Led Đường Phố', 'manhtien15091@gmail.com', '0932362666', '123', '2021-09-11 17:38:51', '2021-09-11 17:38:51', 1);
INSERT INTO `khaosatvien` VALUES (2, 'Toàn thắng', 'homi1co.tbd1@gmail.com', '0932362661', 'bcv', '2021-09-11 17:39:52', '2021-09-11 17:40:13', 1);

-- ----------------------------
-- Table structure for menu_admin
-- ----------------------------
DROP TABLE IF EXISTS `menu_admin`;
CREATE TABLE `menu_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `parent_id` int(11) NULL DEFAULT NULL,
  `order` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_admin
-- ----------------------------
INSERT INTO `menu_admin` VALUES (10, 'Hệ thống', 'admin/system', 'fa-users', 1, '2021-07-19 11:37:57', '2021-09-12 11:17:32', NULL, 5);
INSERT INTO `menu_admin` VALUES (11, 'Danh sách người dùng', 'admin/system/users/list', NULL, 1, '2021-07-19 11:38:57', '2021-09-11 10:15:12', 10, NULL);
INSERT INTO `menu_admin` VALUES (26, 'Lịch sử người dùng', 'admin/system/users/user_history', NULL, 1, '2021-09-10 22:31:01', '2021-09-11 10:15:15', 10, NULL);
INSERT INTO `menu_admin` VALUES (27, 'Điều tra viên', 'admin/coroner', 'fa-id-card', 1, '2021-09-11 10:15:54', '2021-09-11 10:15:56', NULL, 1);
INSERT INTO `menu_admin` VALUES (28, 'Danh sách điều tra viên', 'admin/coroner/list', NULL, 1, '2021-09-11 10:15:57', '2021-09-11 10:16:59', 27, NULL);
INSERT INTO `menu_admin` VALUES (29, 'Loại tội phạm', 'admin/criminal', 'fa-gavel', 1, '2021-09-11 12:01:15', '2021-09-11 12:01:17', NULL, 2);
INSERT INTO `menu_admin` VALUES (30, 'Danh sách loại tội phạm', 'admin/criminal/list', NULL, 1, '2021-09-11 12:01:53', '2021-09-11 12:01:55', 29, NULL);
INSERT INTO `menu_admin` VALUES (31, 'Khảo sát viên', 'admin/surveyor', 'fa-id-card', 1, '2021-09-11 10:15:54', '2021-09-11 10:15:56', NULL, 1);
INSERT INTO `menu_admin` VALUES (32, 'Danh sách khảo sát viên', 'admin/surveyor/list', NULL, 1, '2021-09-11 17:12:32', '2021-09-11 17:12:34', 31, NULL);
INSERT INTO `menu_admin` VALUES (33, 'Quản lý tin báo, vụ án', 'admin/news', 'fas fa-newspaper', 1, '2021-09-11 17:43:17', '2021-09-11 17:45:50', NULL, 3);
INSERT INTO `menu_admin` VALUES (34, 'Danh sách tin báo, vụ án', 'admin/news/list', NULL, 1, '2021-09-11 17:46:16', '2021-09-11 17:46:19', 33, NULL);
INSERT INTO `menu_admin` VALUES (35, 'Báo cáo', 'admin/report', 'fa-file', 1, '2021-09-12 11:17:16', '2021-09-12 11:17:18', NULL, 4);
INSERT INTO `menu_admin` VALUES (36, 'Thống kê, vụ án đang điều tra', 'admin/report/news', NULL, 1, '2021-09-12 11:19:17', '2021-09-12 11:19:19', 35, NULL);

-- ----------------------------
-- Table structure for thongtinvuan
-- ----------------------------
DROP TABLE IF EXISTS `thongtinvuan`;
CREATE TABLE `thongtinvuan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noidungtinbao` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `noidungvuan` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `dtv_id` int(11) NULL DEFAULT NULL,
  `ksv_id` int(11) NULL DEFAULT NULL,
  `toidanh` int(11) NULL DEFAULT NULL,
  `ketquadieutra` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sotinbao` int(1) NULL DEFAULT NULL,
  `ngaybatdautinbao` timestamp(0) NULL DEFAULT NULL,
  `ngayketthuctinbao` timestamp(0) NULL DEFAULT NULL,
  `ngaybatdaugiahantinbao` timestamp(0) NULL DEFAULT NULL,
  `ngaykethucgiahantinbao` timestamp(0) NULL DEFAULT NULL,
  `tamdinhchibaotin` int(11) NULL DEFAULT 0 COMMENT '0: hoat dong binh thuong, 1 dinh chi bao tin',
  `soktva` int(11) NULL DEFAULT NULL,
  `ngaybatdauktva` timestamp(0) NULL DEFAULT NULL,
  `ngaykethucktva` timestamp(0) NULL DEFAULT NULL,
  `tamdinhchivuan` int(11) NULL DEFAULT 0 COMMENT '0: ko dinh chi, 1: dinh chi vu an',
  `khoito` int(11) NULL DEFAULT 0 COMMENT '0: khong khoi to, 1 khoi to',
  `bienphapnganchan` int(11) NULL DEFAULT 0 COMMENT '0: chua co bien phap,1:bat tam giam, 2: tam giam,  3: cam , 4: bao lanh , 5:BPNC khac',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of thongtinvuan
-- ----------------------------
INSERT INTO `thongtinvuan` VALUES (4, '1', '6', NULL, NULL, NULL, '<p>6</p>', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, 1, '2021-09-11 22:12:20', '2021-09-11 22:12:20');
INSERT INTO `thongtinvuan` VALUES (14, 'test', 'test', 7, 1, 3, '<p>6666666666666666666666666666 5646 23424</p>', 3, '2021-09-02 00:00:00', '2021-08-31 00:00:00', '2021-09-22 00:00:00', '2021-09-16 00:00:00', 0, 5, '2021-09-22 00:00:00', '2021-09-15 00:00:00', 0, 1, 4, '2021-09-12 00:16:20', '2021-09-12 20:34:07');

-- ----------------------------
-- Table structure for user_history
-- ----------------------------
DROP TABLE IF EXISTS `user_history`;
CREATE TABLE `user_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_history
-- ----------------------------
INSERT INTO `user_history` VALUES (1, 1, 'Login', 'Đăng nhập', '2021-09-10 15:24:32', '2021-09-10 15:24:32');
INSERT INTO `user_history` VALUES (2, 1, 'Logout', 'Đăng xuất', '2021-09-10 15:27:00', '2021-09-10 15:27:00');
INSERT INTO `user_history` VALUES (3, 1, 'Login', 'Đăng nhập', '2021-09-10 15:27:13', '2021-09-10 15:27:13');
INSERT INTO `user_history` VALUES (4, 1, 'View user', 'Xem danh sách người dùng', '2021-09-10 15:28:39', '2021-09-10 15:28:39');
INSERT INTO `user_history` VALUES (5, 1, 'View user', 'Xem danh sách người dùng', '2021-09-10 15:31:13', '2021-09-10 15:31:13');
INSERT INTO `user_history` VALUES (6, 1, 'View user', 'Xem danh sách người dùng', '2021-09-10 15:54:43', '2021-09-10 15:54:43');
INSERT INTO `user_history` VALUES (7, 1, 'Login', 'Đăng nhập', '2021-09-11 03:07:59', '2021-09-11 03:07:59');
INSERT INTO `user_history` VALUES (8, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:12:28', '2021-09-11 03:12:28');
INSERT INTO `user_history` VALUES (9, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:14:49', '2021-09-11 03:14:49');
INSERT INTO `user_history` VALUES (10, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:15:03', '2021-09-11 03:15:03');
INSERT INTO `user_history` VALUES (11, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:15:19', '2021-09-11 03:15:19');
INSERT INTO `user_history` VALUES (12, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:15:26', '2021-09-11 03:15:26');
INSERT INTO `user_history` VALUES (13, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:16:08', '2021-09-11 03:16:08');
INSERT INTO `user_history` VALUES (14, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 03:17:01', '2021-09-11 03:17:01');
INSERT INTO `user_history` VALUES (15, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 04:56:17', '2021-09-11 04:56:17');
INSERT INTO `user_history` VALUES (16, 1, 'Add user', 'Thêm người dùng', '2021-09-11 04:56:36', '2021-09-11 04:56:36');
INSERT INTO `user_history` VALUES (17, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 04:56:36', '2021-09-11 04:56:36');
INSERT INTO `user_history` VALUES (18, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 05:05:48', '2021-09-11 05:05:48');
INSERT INTO `user_history` VALUES (19, 1, 'Xóa user', 'Xóa người dùng', '2021-09-11 05:05:57', '2021-09-11 05:05:57');
INSERT INTO `user_history` VALUES (20, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 05:05:57', '2021-09-11 05:05:57');
INSERT INTO `user_history` VALUES (21, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 05:05:59', '2021-09-11 05:05:59');
INSERT INTO `user_history` VALUES (22, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:25', '2021-09-11 12:59:25');
INSERT INTO `user_history` VALUES (23, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:27', '2021-09-11 12:59:27');
INSERT INTO `user_history` VALUES (24, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:31', '2021-09-11 12:59:31');
INSERT INTO `user_history` VALUES (25, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:33', '2021-09-11 12:59:33');
INSERT INTO `user_history` VALUES (26, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:36', '2021-09-11 12:59:36');
INSERT INTO `user_history` VALUES (27, 1, 'Add user', 'Thêm người dùng', '2021-09-11 12:59:52', '2021-09-11 12:59:52');
INSERT INTO `user_history` VALUES (28, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:53', '2021-09-11 12:59:53');
INSERT INTO `user_history` VALUES (29, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:56', '2021-09-11 12:59:56');
INSERT INTO `user_history` VALUES (30, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:57', '2021-09-11 12:59:57');
INSERT INTO `user_history` VALUES (31, 1, 'View user', 'Xem danh sách người dùng', '2021-09-11 12:59:58', '2021-09-11 12:59:58');
INSERT INTO `user_history` VALUES (32, 1, 'Login', 'Đăng nhập', '2021-09-11 17:06:18', '2021-09-11 17:06:18');
INSERT INTO `user_history` VALUES (33, 1, 'Login', 'Đăng nhập', '2021-09-12 11:16:21', '2021-09-12 11:16:21');
INSERT INTO `user_history` VALUES (34, 1, 'Login', 'Đăng nhập', '2021-09-12 19:37:08', '2021-09-12 19:37:08');
INSERT INTO `user_history` VALUES (35, 1, 'Login', 'Đăng nhập', '2021-09-13 08:42:06', '2021-09-13 08:42:06');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FirstName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `LastName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` bigint(20) NULL DEFAULT NULL,
  `last_login` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Admin Super', 'manhtien1509@gmail.com', 'Admin', '$2y$10$egxEA1vRiHuc0fl2ebvI/.USGg46TiUhsHZAg749dET1AUifU1sv.', 'sy9B3UHJ6VEllnjf3uKuhT41v56CwTlJzndrhWH9glWg5tbuK1KC1c0Pn6hQ', '0000-00-00 00:00:00', '2021-09-13 08:42:06', 'Super', 1, '2021-09-13 08:42:06');
INSERT INTO `users` VALUES (41, 'a b', 'manhtien15091@gmail.com', 'a', '$2y$10$sJBE2sw.yUMJEZGVr7D1ueUBilFzj9JvO3/A1o9r/PaKLpTxZXt9i', NULL, '2021-09-10 13:56:04', '2021-09-10 14:07:24', 'b', 2, NULL);
INSERT INTO `users` VALUES (45, 'Hoang 1', 'manhtien150912@gmail.com', 'Hoang', '$2y$10$eCqwqdsosYBuemxe33Piwe739soMuOsMpvOt1lOUyzr82w2Gj1lXK', NULL, '2021-09-11 12:59:52', '2021-09-11 12:59:52', '1', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
